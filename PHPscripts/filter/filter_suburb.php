<?php
require_once '../class/filter_suburb.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/filter/filter_suburb.php
 *
 */

if (isset($_POST['city_id'])) {

    $city_id = $_POST['city_id'];
	$db_operarion = new FilterSuburb();
	$db_operarion->findByCity($city_id);
}
