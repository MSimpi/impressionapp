<?php
require_once '../class/usersql.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/user/updateUserRole.php
 *
 */
 
$response = array();
$response["sqlflag"] = FALSE;

if (isset($_POST['uid'])
    && isset($_POST['featureid'])) {

    $db_operarion = new UserSQL();
    

    $uid = $_POST['uid'];
    $featureid = $_POST['featureid'];
   
    
	if ($db_operarion->updateUserRole($uid, $featureid) === true) {
		$response["sqlflag"] = TRUE;
		$response["message"] = "User Role Updated successfully";
	} else {
	
		$response["message"] = "Register Error: sql execute wrong.";
	}
} else {
    $response["message"] = 'Check request input value.';
}

echo json_encode($response);