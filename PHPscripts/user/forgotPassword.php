<?php
require_once '../class/usersql.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/user/forgotPassword.php
 *
 */

$response = array();
$response["sqlflag"] = FALSE;
 
if (isset($_POST['email']) && isset($_POST['new_password'])) {

    $db_operarion = new UserSQL();

    $email = $_POST['email'];
	$new_password = $_POST['new_password'];
	
	if($db_operarion->checkExist($email)){
		$db_operarion->updateUserForgotPassword($email, $new_password);
		$response["sqlflag"] = TRUE;
	}else{
		$response["message"] = "Email Not Found !!";
	}		
}

echo json_encode($response);