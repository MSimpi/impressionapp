<?php
require_once '../class/usersql.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/user/updatePassword.php
 *
 */

if (isset($_POST['uid'])) {

    $db_operarion = new UserSQL();
    

    $uid = $_POST['uid'];
    $cur_password = $_POST['cur_password'];
    $new_password = $_POST['new_password'];
    
    $db_operarion->updateUserPassword($uid, $new_password);   
}