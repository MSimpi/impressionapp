<?php
require_once '../class/usersql.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/user/register.php
 *
 */
 
 
$response = array();
$response["sqlflag"] = FALSE;

if (isset($_POST['email'])
    && isset($_POST['password']) ) {

    $db_operarion = new UserSQL();
    

    //$uid = $_POST['uid'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $suburb = $_POST['suburb'];
    $postcode = $_POST['postcode'];
    $address = $_POST['address'];
    //$feature_id = $_POST['feature_id'];
    //$usertype = $_POST['usertype'];
    //$country = $_POST['country'];

    if ($db_operarion->checkExist($email)) {
        $response["message"] = "User already exists !!";
    } else {
        //$uid, $fstname, $lstname, $email, $phone, $pwd
        if ($db_operarion->createUser($firstName, $lastName, $email, $password, $suburb, $postcode, $address) === true) {
            $response["sqlflag"] = TRUE;
            $response["message"] = "User signup successfully";
        } else {
        
            $response["message"] = "Register Error: sql execute wrong.";
        }
    }
} else {
    $response["message"] = 'Check request input value.';
}

echo json_encode($response);

