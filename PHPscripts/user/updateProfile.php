<?php
require_once '../class/usersql.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/user/updateProfile.php
 *
 */

if (isset($_POST['uid'])) {

    $db_operarion = new UserSQL();
    

    $uid = $_POST['uid'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $suburb = $_POST['suburb'];
    $postcode = $_POST['postcode'];
    $address = $_POST['address'];
    

    
    if ($db_operarion->updateUser($uid, $firstname, $lastname, $email, $phone, $suburb, $postcode, $address) === true) {
        $response["sqlflag"] = TRUE;
        $response["message"] = "User Updated successfully";
    } else {
        
        $response["message"] = "Update User Error: sql execute wrong.";
    }
    
    echo json_encode($response);
}