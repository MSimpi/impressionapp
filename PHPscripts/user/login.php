<?php
require_once '../class/usersql.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/user/login.php
 *
 */

if (isset($_POST['email']) && isset($_POST['pwd'])) {

    $db_operarion = new UserSQL();
    $response = array();
    $response["sqlflag"] = FALSE;

    $email = $_POST['email'];
    $pwd = $_POST['pwd'];

    $db_operarion->checkLogin($email, $pwd);   
}