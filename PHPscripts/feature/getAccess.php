<?php
require_once '../class/feature.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/feature/getAccess.php
 *
 */

 if (isset($_POST['feature_id'])) {

    $db_operarion = new FeatureSQL();
    $response = array();

    $user_group = $_POST['feature_id'];

    if ($db_operarion->getListByAccess($user_group)) {
        $response["message"] = "Something Went Wrong!";
    } else {
        return;
    }
} else {
    $response["message"] = "User role is missing!";
}

echo json_encode($response);