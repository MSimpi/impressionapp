<?php
require_once '../class/property.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/property/property_getComparisonList.php
 *
 */

 if (isset($_POST['comparisonlist'])) {

    $db_operarion = new PropertySQL();
    $response = array();
    http_response_code(404);

    $comparisonlist = $_POST['comparisonlist'];

    if ($db_operarion->getComparisonList($comparisonlist)) {
        http_response_code(200);
        $response["message"] = "Something Went Wrong!";
    } else {
        $response["message"] = "getListDetails: sql execute wrong.";
    }
} else {
    $response["message"] = "Property Details Loaded Successfully.";
}

echo json_encode($response);

