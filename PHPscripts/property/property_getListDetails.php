<?php
require_once '../class/property.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/property/property_getListDetails.php
 *
 */

 if (isset($_POST['id'])) {

    $db_operarion = new PropertySQL();
    $response = array();
    http_response_code(404);

    $id = $_POST['id'];

    if ($db_operarion->getListDetails($id)) {
        http_response_code(200);
        $response["message"] = "Something Went Wrong!";
    } else {
        $response["message"] = "getListDetails: sql execute wrong.";
    }
} else {
    $response["message"] = "Property Details Loaded Successfully.";
}

echo json_encode($response);

