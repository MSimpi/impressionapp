<?php
require_once '../class/agent.class.php';

/**
 * ENDPOINT: http://impression-real-estate.co.nf/property/property_getAgentDetails.php
 *
 */

 if (isset($_POST['agentid'])) {

    $db_operarion = new AgentSQL();

    $agentid = $_POST['agentid'];

    $db_operarion->getAgentDetails($agentid);
}