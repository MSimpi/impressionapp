<?php

class FilterCity
{
    //City TABLE CONSTANTS
    const TABLE_NAME   = 'tbl_city';
    const COL1_CITYID = 'city_id';
    const COL2_CITYNAME = 'city_name';

    private $conn;

    function __construct()
    {
        require_once '../config/db_connect.class.php';
        $db = new DB_Connect();
        $this->conn = $db->getConnection();
    }

    public function getCity()
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::TABLE_NAME;
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }

}