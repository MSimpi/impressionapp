<?php

class SampleSQL
{
    //Sample TABLE CONSTANTS
    const SAMP_TABLE_NAME   = '..';
    const SAMP_COL1_UID     = '..';
    const SAMP_COL2_FSTNAME = '';
    const SAMP_COL3_LSTNAME = '';
    const SAMP_COL4_EMAIL   = '';
    const SAMP_COL5_PHONE   = '';
    const SAMP_COL6_PWD     = '';
    //7 8  .... 13

    private $conn;

    function __construct()
    {
        require_once '../config/db_connect.class.php';
        $db = new DB_Connect();
        $this->conn = $db->getConnection();
    }

    public function findAll()
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::SAMP_TABLE_NAME;
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }

}