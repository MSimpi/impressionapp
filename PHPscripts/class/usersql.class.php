<?php

class UserSQL
{
    //USER TABLE CONSTANTS
    const USER_TABLE_NAME   = 'tbl_user';
    const USER_COL1_UID     = 'user_id';
    const USER_COL2_FSTNAME = 'first_name';
    const USER_COL3_LSTNAME = 'last_name';
    const USER_COL4_EMAIL   = 'email';
    const USER_COL5_PHONE   = 'phone';
    const USER_COL6_PWD     = 'password';
    const USER_COL7_FEATURE_ID = 'feature_id';
    const USER_COL8_STATUS = 'status';
    const USER_COL9_ADDRESS = 'address';
    const USER_COL10_SUBURB = 'suburb';
    const USER_COL11_POSTCODE = 'postcode';
    
    //FEATURE TABLE CONSTANTS
    const FEATURE_TABLE_NAME   = 'tbl_feature';
    const FEATURE_COL1_ID     = 'feature_id';
    const FEATURE_COL2_VALUE = 'feature_value';
    const FEATURE_COL3_GROUP = 'feature_group';
    const FEATURE_COL3_STATUS = 'status';

    private $conn;

    function __construct()
    {
        require_once '../config/db_connect.class.php';
        $db = new DB_Connect();
        $this->conn = $db->getConnection();
    }

    public function checkExist($email)
    {
        $query = $this->conn->prepare('SELECT * FROM ' . self::USER_TABLE_NAME . ' WHERE ' . self::USER_COL4_EMAIL . " = ?");
        $query->bind_param("s", $email);
        $query->execute();
        $query->store_result();

        if ($query->num_rows > 0) {
            $query->close();
            return true;
        } else {
            $query->close();
            return false;
        }
    }

    public function findPwdByName($uname)
    {
        $query = 'SELECT ' . self::USER_COL6_PWD
            . ' FROM ' . self::USER_TABLE_NAME
            . ' WHERE ' . self::USER_COL4_EMAIL . " = '$uname' limit 1";

        if ($result = mysqli_query($this->conn, $query)) {
            $row = mysqli_fetch_assoc($result);
            mysqli_free_result($result);
            return $row['UserPassword'];
        }
    }

    public function findAll()
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::USER_TABLE_NAME;
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }

    public function createUser($firstName, $lastName, $email, $pwd, $suburb, $postcode, $address)
    {
        $query = $this->conn->prepare('INSERT INTO '
            . self::USER_TABLE_NAME . '('
            . self::USER_COL2_FSTNAME . ', '
            . self::USER_COL3_LSTNAME . ', '
            . self::USER_COL4_EMAIL . ', '
            . self::USER_COL9_ADDRESS . ', '
            . self::USER_COL10_SUBURB . ', '
            . self::USER_COL11_POSTCODE . ', '
            . self::USER_COL6_PWD 
            . ") VALUES (?,?,?,?,?,?,?)");
        $query->bind_param("sssssss", $firstName, $lastName, $email, $address, $suburb, $postcode, $pwd);

        if ($query->execute() === TRUE) {
            return true;
        } else {
            return false;
        }
        $query->close();
    }
    
    public function getUser($uid)
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::USER_TABLE_NAME." WHERE ". self::USER_COL1_UID . " = '$uid'";
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }

    public function updateUser($uid, $firstname, $lastname, $email, $phone, $suburb, $postcode, $address)
    {
        $query = $this->conn->prepare('UPDATE '. self::USER_TABLE_NAME. ' SET '. self::USER_COL2_FSTNAME ." = '".$firstname."', ".self::USER_COL3_LSTNAME . " = '".$lastname."', ".self::USER_COL4_EMAIL . " = '".$email."', ".self::USER_COL5_PHONE . " = '".$phone."', ". self::USER_COL9_ADDRESS . " = '".$address."', ". self::USER_COL10_SUBURB . " = '".$suburb."', ". self::USER_COL11_POSTCODE . " = '".$postcode."' WHERE ". self::USER_COL1_UID . " = ".$uid);

        if ($query->execute() === TRUE) {
            return true;
        } else {
            return false;
        }
        $query->close();
    }

    public function updateUserPassword($uid, $password)
    {
        $query = $this->conn->prepare('UPDATE '. self::USER_TABLE_NAME. ' SET '. self::USER_COL6_PWD ." = '".$password."' WHERE ". self::USER_COL1_UID . " = ".$uid);

        if ($query->execute() === TRUE) {
            return true;
        } else {
            return false;
        }
        $query->close();
    }
    
    public function updateUserForgotPassword($email, $password)
    {
        $query = $this->conn->prepare('UPDATE '. self::USER_TABLE_NAME. ' SET '. self::USER_COL6_PWD ." = '".$password."' WHERE ". self::USER_COL4_EMAIL . " = '".$email."'");

        if ($query->execute() === TRUE) {
            return true;
        } else {
            return false;
        }
        $query->close();
    }
    
    public function updateUserRole($uid, $featureid)
    {
        $query = $this->conn->prepare('UPDATE '
            . self::USER_TABLE_NAME
            . ' SET '
            . self::USER_COL12_FEATURE . " = '$featureid' WHERE "
            . self::USER_COL1_UID . " = '$uid'");

        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
        $query->close();
    }
    
    public function checkLogin($email, $pwd)
    {
        $query = "SELECT * FROM ".self::USER_TABLE_NAME.
                 " WHERE ".SELF::USER_COL4_EMAIL . " = '".$email.
                 "' AND ".SELF::USER_COL6_PWD. " = '".$pwd.
                 "' AND ".SELF::USER_COL8_STATUS. " = 'active' LIMIT 1";

        $result = mysqli_query($this->conn, $query);

        if ($result->num_rows > 0) {
        
            while ($row = mysqli_fetch_assoc($result)) {
                    $arr_str .= json_encode($row);
                    $arr_str .= ',';
                }
            $str_result = rtrim($arr_str, ",");
            echo "[$str_result]";
        }
    }
}