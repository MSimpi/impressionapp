<?php

class FilterSuburb
{
    //Suburb TABLE CONSTANTS
    const SUB_TABLE_NAME   = 'tbl_suburb';
    const SUB_COL1_SUBURBID = 'suburb_id';
    const SUB_COL2_SUBURBNAME = 'suburb_name';
    const SUB_COL3_CITYID = 'city_id';

    private $conn;

    function __construct()
    {
        require_once '../config/db_connect.class.php';
        $db = new DB_Connect();
        $this->conn = $db->getConnection();
    }

    public function findByCity($CityId)
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::SUB_TABLE_NAME .' WHERE '.self::SUB_COL3_CITYID. " = $CityId";
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }

}