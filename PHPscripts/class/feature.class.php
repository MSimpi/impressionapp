<?php

class FeatureSQL
{
    //FEATURE TABLE CONSTANTS
    const FEATURE_TABLE_NAME   = 'tbl_feature';
    const FEATURE_COL1_ID     = 'feature_id';
    const FEATURE_COL2_VALUE = 'feature_value';
    const FEATURE_COL3_GROUP = 'feature_group';
	const FEATURE_COL3_STATUS = 'status';
	
	//FEATURE VALUE TABLE CONSTANTS
    const FEATURE_VALUE_TABLE_NAME   = 'tbl_feature_value';
    const FEATURE_VALUE_COL1_ID     = 'feature_value_id';
    const FEATURE_VALUE_COL2_FID     = 'feature_id';
    const FEATURE_VALUE_COL3_TEXT = 'feature_text_value';
	const FEATURE_VALUE_COL4_KEY = 'feature_key_value';
	
	//FEATURE ACCESS TABLE CONSTANTS
    const FEATURE_ACCESS_TABLE_NAME   = 'tbl_feature_access';
    const FEATURE_ACCESS_COL1_ID     = 'feature_access_id';
    const FEATURE_ACCESS_COL2_GROUP = 'feature_group';
	const FEATURE_ACCESS_COL3_UID = 'feature_U_id';
	const FEATURE_ACCESS_COL4_FID = 'feature_F_id';
	
    private $conn;

    function __construct()
    {
        require_once '../config/db_connect.class.php';
        $db = new DB_Connect();
        $this->conn = $db->getConnection();
    }
	
	public function getListByAccess($uid)
    {
        $arr_str = '';
        $query = 'SELECT DISTINCT * FROM ' . self::FEATURE_ACCESS_TABLE_NAME.' INNER JOIN '.self::FEATURE_TABLE_NAME.' ON '.self::FEATURE_ACCESS_TABLE_NAME.'.'.self::FEATURE_ACCESS_COL4_FID.'='.self::FEATURE_TABLE_NAME.'.'.self::FEATURE_COL1_ID.' WHERE '.self::FEATURE_ACCESS_COL3_UID.'='.$uid;
        
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }
	
	public function getListByValue()
    {
        $arr_str = '';
		//SELECT * FROM `tbl_feature_value` INNER JOIN `tbl_feature` ON `tbl_feature_value`.`feature_id`=`tbl_feature`.`feature_id`
        $query = 'SELECT * FROM ' . self::FEATURE_VALUE_TABLE_NAME.' LEFT JOIN '.self::FEATURE_TABLE_NAME.' ON '.self::FEATURE_VALUE_TABLE_NAME.'.'.self::FEATURE_VALUE_COL2_FID.'='.self::FEATURE_TABLE_NAME.'.'.self::FEATURE_COL1_ID;

        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }
}