<?php

class AgentSQL
{
    //AGENT TABLE CONSTANTS
    const TABLE_NAME_AGENT   = 'staff_members';
    const COL1_AID = 'id';
    const COL2_AGENT_FNAME = 'first_name';
	const COL3_AGENT_LNAME = 'last_name';
    const COL4_AGENT_EMAIL   = 'email';
    const COL5_AGENT_MOBILE   = 'mobile';
	const COL6_AGENT_TEAM   = 'team';
	const COL7_AGENT_POSITION   = 'position';
	const COL8_AGENT_THUMB_IMAGE   = 'thumb_image';
	const COL9_AGENT_CONTENT   = 'content';
	const COL10_AGENT_QUOTE   = 'quote';
	const COL11_AGENT_PHONE   = 'phone';
	
    private $conn;

    function __construct()
    {
        require_once '../config/db_connect.class.php';
        $db = new DB_Connect();
        $this->conn = $db->getConnection();
    }

	public function getAgentDetails($agentid)
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::TABLE_NAME_AGENT." WHERE ". self::COL1_AID . " = '$agentid'";
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }
}