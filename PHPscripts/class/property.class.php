<?php

class PropertySQL
{
    //PROPERTY TABLE CONSTANTS
    const TABLE_NAME   = 'sales';
    const COL1_ID     = 'id';
    const COL2_TITLE = 'title';
    const COL3_DESCRIPTION = 'description';
    const COL4_UNIQUE_ID = 'unique_id';
    const COL5_AGENT_ID = 'agent_id';
    const COL6_MAIN_IMAGE = 'main_image';
    const COL7_IMAGES = 'images';
    const COL8_STATUS = 'status';
    const COL9_PRICE = 'price';
    const COL10_AUCTION = 'auction';
    const COL11_AUCTION_DATE = 'auction_date';
    const COL12_PRICEUPONAPPLICATION = 'price_upon_application';
    const COL13_BEDROOMS = 'bedrooms';
    const COL14_BATHROOM = 'bathrooms';
    const COL15_CARPORTS = 'carports';
    const COL16_GARAGES = 'garages';
    const COL17_AIR_CONDITION = 'air_conditioning';
    const COL18_POOL = 'pool';
    const COL19_ALARM = 'alarm_system';
    const COL20_FEATURED = 'featured';
    const COL21_TYPE = 'type';
    const COL22_COUNCIL_RATES = 'council_rates';
    const COL23_WATER_RATES = 'water_rates';
    const COL24_STRATA_RATES = 'strata_rates';
    const COL25_RANTAL_TERM = 'rental_term';
    const COL26_RENT_PER_WEEK = 'rent_per_week';
    const COL27_SQUARE_METER = 'square_meter';
    const COL28_STREET_NUMBER = 'street_number';
    const COL29_STREET = 'street';
    const COL30_SUBURB = 'suburb';
    const COL31_REGION = 'region';
    const COL32_COUNTRY = 'country';
    
    private $conn;

    function __construct()
    {
        require_once '../config/db_connect.class.php';
        $db = new DB_Connect();
        $this->conn = $db->getConnection();
    }

    public function getList()
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::TABLE_NAME;
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $row["main_image"] = 'https://impression.co.nz/'.$row["main_image"].'_sm.jpg';

            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }
    
    public function getListDetails($id)
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::TABLE_NAME. ' WHERE '.self::COL1_ID.'='.$id;
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }
	
	public function getComparisonList($comparisonlist)
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::TABLE_NAME. ' WHERE '.self::COL1_ID.' IN ('.$comparisonlist.')';
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }

    public function getSearchList($searchtext, $bedroom, $bathroom, $streetnumber, $street, $suburb, $region)
    {
        $arr_str = '';
        $query = 'SELECT * FROM ' . self::TABLE_NAME. ' WHERE '.
                  self::COL2_TITLE.' LIKE "%'.$searchtext.'%" OR '.
                  self::COL3_DESCRIPTION.' LIKE "%'.$searchtext.'%" OR '.
                  self::COL4_UNIQUE_ID.' LIKE "%'.$searchtext.'%" OR '.
          self::COL13_BEDROOMS.' = "'.$bedroom.'" AND '.
          self::COL14_BATHROOM.' = "'.$bathroom.'" AND '.
          self::COL28_STREET_NUMBER.' LIKE "%'.$streetnumber.'%" AND '.
                  self::COL29_STREET.' LIKE "%'.$street.'%"AND '.
          self::COL30_SUBURB.' LIKE "%'.$suburb.'%" AND '.
          self::COL31_REGION.' LIKE "%'.$region.'%"';
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $arr_str .= json_encode($row);
            $arr_str .= ',';
        }
        $str_result = rtrim($arr_str, ",");
        echo "[$str_result]";
    }
}