﻿using Android.Content;
using Android.Graphics.Drawables;
using ImpressionApp.Droid;
using ImpressionApp.Services;
using Xamarin.Forms;
using Android.Graphics;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;
using ImpressionApp.Droid.CustomControl;

[assembly: ExportRenderer(typeof(CustomSwitch), typeof(XSwitchRenderer))]
namespace ImpressionApp.Droid.CustomControl
{
    public class XSwitchRenderer : SwitchRenderer
    {

        private Color greyColor = new Color(215, 218, 220);
        private Color greenColor = new Color(32, 156, 68);
        private Color _trueColor = new Color(166, 194, 174);
        private Color _falseColor = new Color(0, 0, 0);

        public XSwitchRenderer(Context context) : base(context)
        {

        }

        protected override void Dispose(bool disposing)
        {
            this.Control.CheckedChange -= this.OnCheckedChange;
            base.Dispose(disposing);
        }


        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Switch> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                if (this.Control.Checked)
                {
                    this.Control.ThumbDrawable.SetColorFilter(greenColor, PorterDuff.Mode.SrcAtop);
                }
                else
                {
                    this.Control.ThumbDrawable.SetColorFilter(greyColor, PorterDuff.Mode.SrcAtop);
                }

                this.Control.CheckedChange += this.OnCheckedChange;

            }
        }

        private void OnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            if (this.Control.Checked)
            {
                this.Control.ThumbDrawable.SetColorFilter(greenColor, PorterDuff.Mode.SrcAtop);
                this.Control.TrackDrawable.SetColorFilter(_trueColor, PorterDuff.Mode.Multiply);
            }
            else
            {
                this.Control.ThumbDrawable.SetColorFilter(greyColor, PorterDuff.Mode.SrcAtop);
                this.Control.TrackDrawable.SetColorFilter(_falseColor, PorterDuff.Mode.Multiply);
            }
        }

    }
}
