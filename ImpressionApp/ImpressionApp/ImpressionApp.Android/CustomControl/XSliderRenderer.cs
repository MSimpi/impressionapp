﻿using System;
using ImpressionApp.Services;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ImpressionApp.Droid.CustomControl;
using Android.Graphics;

[assembly: ExportRenderer(typeof(CustomSlider), typeof(XSliderRenderer))]
namespace ImpressionApp.Droid.CustomControl
{
    public class XSliderRenderer : SliderRenderer
    {
        private CustomSlider view;
        protected override void OnElementChanged(ElementChangedEventArgs<Slider> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                view = (CustomSlider)Element;

                if (view.ThumbColor != Xamarin.Forms.Color.Default || 
                    view.MaxColor != Xamarin.Forms.Color.Default ||
                    view.MinColor != Xamarin.Forms.Color.Default)
                {
                    Control.Thumb.SetColorFilter(view.ThumbColor.ToAndroid(), PorterDuff.Mode.SrcIn);
                    Control.ProgressTintList = Android.Content.Res.ColorStateList.ValueOf(view.MinColor.ToAndroid());
                    Control.ProgressTintMode = PorterDuff.Mode.SrcIn;
                    //this is for Maximum Slider line Color  
                    Control.ProgressBackgroundTintList = Android.Content.Res.ColorStateList.ValueOf(view.MaxColor.ToAndroid());
                    Control.ProgressBackgroundTintMode = PorterDuff.Mode.SrcIn;
                }
                    
            }
        }
    }
}