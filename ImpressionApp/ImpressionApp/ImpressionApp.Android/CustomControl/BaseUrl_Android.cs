﻿using System;
using ImpressionApp.Droid.CustomControl;
using ImpressionApp.Views.Pages;
using Xamarin.Forms;

[assembly: Dependency(typeof(BaseUrl_Android))]
namespace ImpressionApp.Droid.CustomControl
{
    public class BaseUrl_Android : IBaseUrl
    {
        public string Get()
        {
            return "file:///android_asset/";
        }

    }
}
