﻿
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.OS;
using Acr.UserDialogs;
using Xamarin.Forms;
using Android.Support.V4.Content;
using Sharpnado.Presentation.Forms.Droid;
using FFImageLoading.Forms.Platform;
using PanCardView.Droid;
using LabelHtml.Forms.Plugin.Droid;
using Plugin.Permissions;

namespace ImpressionApp.Droid
{
    [Activity(Label = "Impression", Icon = "@drawable/Icon", Theme = "@style/MyTheme.Splash", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            Xamarin.FormsMaps.Init(this, bundle);
            UserDialogs.Init(this);
            Xamarin.Essentials.Platform.Init(this, bundle);
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(enableFastRenderer: true);

            CachedImageRenderer.Init(true);
            CardsViewRenderer.Preserve();

            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);


            SharpnadoInitializer.Initialize();

            HtmlLabelRenderer.Initialize();
            global::Xamarin.Forms.Forms.Init(this, bundle);

            var barBG = new global::Android.Graphics.Color(ContextCompat.GetColor(Forms.Context, Resource.Color.colorPrimaryDark));
            Window.SetStatusBarColor(barBG);
            LoadApplication(new App());
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

