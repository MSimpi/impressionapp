﻿using System;
using Xamarin.Forms;
using ImpressionApp.Views;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace ImpressionApp
{
	public partial class App : Application
	{
		
		public App ()
		{
			InitializeComponent();

            ////test for main function
            MainPage = new MainPage();

            //test for splash page
            //MainPage = new Pages.Splash_Page();
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
