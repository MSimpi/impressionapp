﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public class Agent
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string team { get; set; }
        public string position { get; set; }
        public string thumb_image { get; set; }
        public string content { get; set; }
        public string quote { get; set; }
        public string phone { get; set; }
    }
}
