﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public class Feature_Value
    {
        public string feature_value_id { get; set; }
        public string feature_id { get; set; }
        public string feature_text_value { get; set; }
        public string feature_key_value { get; set; }
        public string feature_value { get; set; }
        public string feature_key_value_low { get; set; } = "";
        public string feature_key_value_high { get; set; } = "";

        public Feature_Value ()
        {

        }

        //mock data
        public List<Feature_Value> GetFeatureValues()
        {
            List<Feature_Value> valueList = new List<Feature_Value>();

            ////fetch data from DB API
            ////TODO

            //valueList.Add(new FeatureValue(GlobalVar.FLT02_BEDROOM, "0", "Any"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT02_BEDROOM, "1", "1"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT02_BEDROOM, "2", "2"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT02_BEDROOM, "3", "3"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT02_BEDROOM, "4", "4"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT02_BEDROOM, "5", "5"));

            //valueList.Add( new FeatureValue(GlobalVar.FLT03_BATHROOM, "0", "Any"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT03_BATHROOM, "1", "1"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT03_BATHROOM, "2", "2"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT03_BATHROOM, "3", "3"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT03_BATHROOM, "4", "4"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT03_BATHROOM, "5", "5"));

            //valueList.Add( new FeatureValue(GlobalVar.FLT04_PRICE, "Any price", "0"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT04_PRICE, "Under $300K", "1"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT04_PRICE, "$301K to $500K", "2"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT04_PRICE, "Above $500K", "3"));

            //valueList.Add( new FeatureValue(GlobalVar.FLT06_YEARBUILT, "Any Year", "0"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT06_YEARBUILT, "Before 1960", "1"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT06_YEARBUILT, "1960 to 1980", "2"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT06_YEARBUILT, "1980 to 2000", "3"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT06_YEARBUILT, "After 2000", "4"));

            //valueList.Add( new FeatureValue(GlobalVar.FLT08_SORTING, "Lowest Price", "1"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT08_SORTING, "Highest price", "2"));
            //valueList.Add( new FeatureValue(GlobalVar.FLT08_SORTING, "Latest listed", "3"));


            //valueList.Add(new FeatureValue(GlobalVar.FLT09_SQUAREM, "Any", "0"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT09_SQUAREM, "Under 40 sqm", "1"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT09_SQUAREM, "40 to 50 sqm", "2"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT09_SQUAREM, "50 to 65 sqm", "3"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT09_SQUAREM, "Over 65 sqm", "4"));

            //valueList.Add(new FeatureValue(GlobalVar.FLT10_LEVYCHARGE, "Under 4K", "1"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT10_LEVYCHARGE, "4K to 5K", "2"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT10_LEVYCHARGE, "5K to 6K", "3"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT10_LEVYCHARGE, "Over 6K", "4"));

            //valueList.Add(new FeatureValue(GlobalVar.FLT11_TIMEFRAME, "1 to 3 months", "1"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT11_TIMEFRAME, "3 to 6 months", "2"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT11_TIMEFRAME, "6 to 12 months", "3"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT11_TIMEFRAME, "Over 12 months", "4"));

            //valueList.Add(new FeatureValue(GlobalVar.FLT12_SWIMPOOL,    "0", "Any"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT12_SWIMPOOL,    "1", "Yes"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT12_SWIMPOOL,    "2", "No"));

            //valueList.Add(new FeatureValue(GlobalVar.FLT15_REMISSUE,    "0", "Any"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT15_REMISSUE,    "1", "Existing"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT15_REMISSUE,    "2", "Solved"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT15_REMISSUE,    "3", "Never"));

            //valueList.Add(new FeatureValue(GlobalVar.FLT16_RENT, "Any", "0"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT16_RENT, "Under $350", "1"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT16_RENT, "$350 to $450", "2"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT16_RENT, "$450 to $550", "3"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT16_RENT, "$550 to $650", "4"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT16_RENT, "Over $650", "5"));

            //valueList.Add(new FeatureValue(GlobalVar.FLT17_YIELD,       "0", "Any"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT17_YIELD,       "1", "Under 5%"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT17_YIELD,       "2", "5% to 6%"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT17_YIELD,       "3", "6% to 7%"));
            //valueList.Add(new FeatureValue(GlobalVar.FLT17_YIELD,       "4", "Over 7%"));

            return valueList;
        }
    }
}
