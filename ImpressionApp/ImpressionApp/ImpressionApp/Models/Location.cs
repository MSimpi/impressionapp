﻿using System.ComponentModel;
using Xamarin.Forms.Maps;
using ImpressionApp.Models;

namespace ImpressionApp
{
    public class Location : INotifyPropertyChanged
    {
        Position _position;

        public string Title { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public Position position { get; set; }
        public Property property { get; set; }

        public Position Position
        {
            get => _position;
            set
            {
                if (!_position.Equals(value))
                {
                    _position = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Position)));
                }
            }
        }

        public Location() { }

        public Location(string address, string title, Property propertyItem)
        {
            Address = address;
            Title = title;
            property = propertyItem;
        }

        public Location(string address, string title, Position position, Property propertyItem)
        {
            Address = address;
            Title = title;
            Position = position;
            property = propertyItem;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
