﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public class PropertyOption
    {
        public int PropertyOptionId { get; set; }
        public string PetFriendly { get; set; }
        public string Gym { get; set; }
        public string AirbnbSuitable { get; set; }
        public string RemediationIssue { get; set; }
        public string Furnished { get; set; }
        public string YearBuilt { get; set; }
        public string LavyCharges { get; set; }
        public string TimeFrameToBuy { get; set; }
        public string Yield { get; set; }
        public int PropertyId { get; set; }
    }
}
