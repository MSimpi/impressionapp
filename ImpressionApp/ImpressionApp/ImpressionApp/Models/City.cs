﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ImpressionApp.Models
{
    public class City
    {
        public int city_id { get; set; }
        public string city_name { get; set; }
    }
}
