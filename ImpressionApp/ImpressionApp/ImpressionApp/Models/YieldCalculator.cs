﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public class YieldCalculator
    {
        public double purchasePrice { get; set; }
        public double rentExpensesWeekly { get; set; }
    }
}
