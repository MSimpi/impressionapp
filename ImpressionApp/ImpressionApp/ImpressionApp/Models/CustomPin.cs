﻿using System;
using Xamarin.Forms.Maps;

namespace ImpressionApp.Models
{
    public class CustomPin : Pin
    {
        public string Url { get; set; }
        public Property property { get; set; }

    }
}
