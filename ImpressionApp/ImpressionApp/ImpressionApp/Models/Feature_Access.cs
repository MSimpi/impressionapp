﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public class Feature_Access
    {
        public string feature_U_id { get; set; }
        public string feature_F_id { get; set; }
        public string feature_id { get; set; }
        public string feature_value { get; set; }
        public string status { get; set; }
    }
}
