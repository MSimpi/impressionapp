﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ImpressionApp.Models
{
    public class Property
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string unique_id { get; set; }
        public string agent_id { get; set; }
        public string main_image { get; set; }
        public string images { get; set; }
        public string status { get; set; }
        public string price { get; set; }
        public string auction { get; set; }
        public string auction_date { get; set; }
        public string price_upon_application { get; set; }
        public string bedrooms { get; set; }
        public string bathrooms { get; set; }
        public string carports { get; set; }
        public string garages { get; set; }
        public string air_conditioning { get; set; }
        public string pool { get; set; }
        public string alarm_system { get; set; }
        public string featured { get; set; }
        public string type { get; set; }
        public string council_rates { get; set; }
        public string water_rates { get; set; }
        public string strata_rates { get; set; }
        public string rental_term { get; set; }
        public string rent_per_week { get; set; }
        public string square_meter { get; set; }
        public string street_number { get; set; }
        public string street { get; set; }
        public string suburb { get; set; }
        public string region { get; set; }
        public string country { get; set; }
        public string listAddress { get; set; }
        public string fullAddress { get; set; }
        public string pricewithsymbol { get; set; }

        public Property()
        {
            title = "Empty";
            fullAddress = "Empty";
        }

    }
}
