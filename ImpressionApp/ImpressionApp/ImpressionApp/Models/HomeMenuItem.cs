﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ImpressionApp.Models
{
    public enum MenuItemType
    {
        Search,
        Profile,
        Enquiry,
        About,
        Tsncs,
        Feedback,
        AboutApp
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }

        public string Image { get; set; }
    }
}
