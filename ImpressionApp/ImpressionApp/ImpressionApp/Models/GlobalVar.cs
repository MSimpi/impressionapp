﻿using ImpressionApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public static class GlobalVar
    {
        //--------------PART 1. FILTERS----------------------
        public const string FLTVALUE_ANY = "Any";

        //basic filter column name
        public const string FLT01_CITY = "city";
        public const string FLT01_SUBURB  = "suburb";
        public const string FLT02_BEDROOM   = "Bedrooms";
        public const string FLT03_BATHROOM  = "Bathrooms";
        public const string FLT04_PRICE     = "Price";
        //public const string FLT05_PET       = "Pet Friendly";
        public const string FLT06_YEARBUILT = "Year Built";
        //public const string FLT07_CARPARKING= "Parking";
        public const string FLT08_SORTING   = "Sorting";

        //advanced filter column name
        //for owners
        public const string FLT09_SQUAREM   = "Square Meter";
        public const string FLT10_LEVYCHARGE= "Levy Charge";
        public const string FLT11_TIMEFRAME = "Time Frame to Buy";
        public const string FLT12_SWIMPOOL  = "Pool";
        public const string FLT13_ALA       = "alarm";
        public const string FLT14_AIRBNB    = "AirBNB Suitable";
        //for investors
        public const string FLT15_REMISSUE  = "Remediation Issue";
        public const string FLT16_RENT      = "Weekly Rent";
        public const string FLT17_YIELD     = "Yield";
        public const string FLT18_FURNISHED = "furnished";

        //new unique filters 18/06/2019
        public const string FLT20_CAR_PORTS = "carports";
        public const string FLT21_GARAGES   = "garages";
        public const string FLT22_RENT_PER_WEEK = "rent_per_week";

        //--------------PART 2. PHP LINKS----------------------
        //PHP API links
        public const string PHP_SERVER = "http://impression-real-estate.co.nf/";
        public const string PHP_FILTER_CITY   = PHP_SERVER + "filter/filter_city.php";
        public const string PHP_FILTER_SUBURB   = PHP_SERVER + "filter/filter_suburb.php";
        public const string PHP_FEATURE_ACCESS  = PHP_SERVER + "feature/getAccess.php";
        public const string PHP_FEATURE_VALUES  = PHP_SERVER + "feature/getValue.php";
        public const string PHP_PROPERTY_ALL = "https://impression.co.nz/api/sales/XCVE-R-YUIG_KLJMN-VBf5_4greht";
        public const string PHP_PROPERTY_SEARCH = PHP_SERVER + "property/property_getSearchList.php";
        public const string PHP_COMPARISON_LIST = PHP_SERVER + "property/property_getComparisonList.php";
        public const string PHP_PROPERTY_AGENT_DETAILS = "https://impression.co.nz/api/staff/XCVE-R-YUIG_KLJMN-VBf5_4greht";

        //--------------PART 3. TABLE CONSTANTS----------------------
        public const string TBL_SUBURB_CITYID = "city_id"; //FilterSuburbTask: for search suburb 
        public const string TBL_FEATURE_ID = "feature_id"; //FeatureAccessTask: for search access
        public const string TBL_AGENT_ID = "agent_id";

        public const string SEARCHTEXT = "searchtext";
        public const string COL1_ID     = "id";
        public const string COL2_TITLE = "title";
        public const string COL3_DESCRIPTION = "description";
        public const string COL4_UNIQUE_ID = "unique_id";
        public const string COL5_AGENT_ID = "agent_id";
        public const string COL6_MAIN_IMAGE = "main_image";
        public const string COL7_IMAGES = "images";
        public const string COL8_STATUS = "status";
        public const string COL9_PRICE = "price";
        public const string COL10_AUCTION = "auction";
        public const string COL11_AUCTION_DATE = "auction_date";
        public const string COL12_PRICEUPONAPPLICATION = "price_upon_application";
        public const string COL13_BEDROOMS = "bedrooms";
        public const string COL14_BATHROOM = "bathrooms";
        public const string COL15_CARPORTS = "carports";
        public const string COL16_GARAGES = "garages";
        public const string COL17_AIR_CONDITION = "air_conditioning";
        public const string COL18_POOL = "pool";
        public const string COL19_ALARM = "alarm_system";
        public const string COL20_FEATURED = "featured";
        public const string COL21_TYPE = "type";
        public const string COL22_COUNCIL_RATES = "council_rates";
        public const string COL23_WATER_RATES = "water_rates";
        public const string COL24_STRATA_RATES = "strata_rates";
        public const string COL25_RANTAL_TERM = "rental_term";
        public const string COL26_RENT_PER_WEEK = "rent_per_week";
        public const string COL27_SQUARE_METER = "square_meter";
        public const string COL28_STREET_NUMBER = "street_number";
        public const string COL29_STREET = "street";
        public const string COL30_SUBURB = "suburb";
        public const string COL31_REGION = "region";
        public const string COL32_COUNTRY = "country";

        //example for global variables
        public static int GlobalValue { get; set; }
        public static bool GlobalBoolean;

        public static SearchFilterViewModel FilterViewModel { get; set; } = new SearchFilterViewModel();

    }
}
