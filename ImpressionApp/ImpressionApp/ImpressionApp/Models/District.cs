﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public class District
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int CityId { get; set; }
    }
}
