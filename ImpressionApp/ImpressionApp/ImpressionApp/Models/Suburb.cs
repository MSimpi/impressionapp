﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace ImpressionApp.Models
{
    public class Suburb
    {
        public int suburb_id { get; set; }
        public string suburb_name { get; set; }
        public int city_id { get; set; }
    }
}
