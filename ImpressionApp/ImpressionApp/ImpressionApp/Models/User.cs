﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Acr.UserDialogs;

namespace ImpressionApp.Models
{
    public class User : PhpMessage
    {
        public int user_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string password { get; set; }
        public string address { get; set; }
        public string suburb { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string status { get; set; }
        public int feature_id { get; set; }

        public User()
        {

        }

        public User(string email, string password, int featureid)
        {
            this.email = email;
            this.password = password;
            this.feature_id = feature_id;
        }

        public PhpMessage createUser(string firstName, string lastName, string email, string password, string suburb, string postcode, string address)
        {

            UserDialogs.Instance.ShowLoading();

            //fetch data from DB API
            WebClient webClient = new WebClient();

            webClient.QueryString.Add("firstName", "" + firstName);
            webClient.QueryString.Add("lastName", "" + lastName);
            webClient.QueryString.Add("email", "" + email);
            webClient.QueryString.Add("password", "" + password);
            webClient.QueryString.Add("suburb", "" + suburb);
            webClient.QueryString.Add("postcode", "" + postcode);
            webClient.QueryString.Add("address", "" + address);
            //webClient.QueryString.Add("feature_id", "" + usertype);

            var result = webClient.UploadValues("http://impression-real-estate.co.nf/user/register.php", "POST", webClient.QueryString);

            string responseString = UnicodeEncoding.UTF8.GetString(result);

            PhpMessage msg = JsonConvert.DeserializeObject<PhpMessage>(responseString);

            UserDialogs.Instance.HideLoading();

            return msg;
        }

        public PhpMessage updateUser(int uid, string firstname, string lastname, string email, string phone, string suburb, string postcode, string address)
        {

            UserDialogs.Instance.ShowLoading();

            //fetch data from DB API
            WebClient webClient = new WebClient();

            webClient.QueryString.Add("uid", "" + uid);
            webClient.QueryString.Add("firstname", "" + firstname);
            webClient.QueryString.Add("lastname", "" + lastname);
            webClient.QueryString.Add("email", "" + email);
            webClient.QueryString.Add("phone", "" + phone);
            webClient.QueryString.Add("suburb", "" + suburb);
            webClient.QueryString.Add("postcode", "" + postcode);
            webClient.QueryString.Add("address", "" + address);

            var result = webClient.UploadValues("http://impression-real-estate.co.nf/user/updateProfile.php", "POST", webClient.QueryString);

            string responseString = UnicodeEncoding.UTF8.GetString(result);

            PhpMessage msg = JsonConvert.DeserializeObject<PhpMessage>(responseString);

            UserDialogs.Instance.HideLoading();

            return msg;
        }

        public PhpMessage updateUserPassword(int uid, string cur_password, string new_password)
        {

            UserDialogs.Instance.ShowLoading();

            //fetch data from DB API
            WebClient webClient = new WebClient();

            webClient.QueryString.Add("uid", "" + uid);
            webClient.QueryString.Add("cur_password", "" + cur_password);
            webClient.QueryString.Add("new_password", "" + new_password);

            var result = webClient.UploadValues("http://impression-real-estate.co.nf/user/updatePassword.php", "POST", webClient.QueryString);

            string responseString = UnicodeEncoding.UTF8.GetString(result);

            PhpMessage msg = JsonConvert.DeserializeObject<PhpMessage>(responseString);

            UserDialogs.Instance.HideLoading();

            return msg;
        }

        public PhpMessage updateUserForgotPassword(string email, string new_password)
        {

            UserDialogs.Instance.ShowLoading();

            //fetch data from DB API
            WebClient webClient = new WebClient();

            webClient.QueryString.Add("email", "" + email);
            webClient.QueryString.Add("new_password", "" + new_password);

            var result = webClient.UploadValues("http://impression-real-estate.co.nf/user/forgotPassword.php", "POST", webClient.QueryString);

            string responseString = UnicodeEncoding.UTF8.GetString(result);

            PhpMessage msg = JsonConvert.DeserializeObject<PhpMessage>(responseString);

            UserDialogs.Instance.HideLoading();

            return msg;
        }

        public List<User> loginUser(string email, string pwd)
        {
            List<User> userList = new List<User>();

            UserDialogs.Instance.ShowLoading();

            //fetch data from DB API
            WebClient webClient = new WebClient();

            webClient.QueryString.Add("email", "" + email);
            webClient.QueryString.Add("pwd", "" + pwd);

            var result = webClient.UploadValues("http://impression-real-estate.co.nf/user/login.php", "POST", webClient.QueryString);

            string responseString = UnicodeEncoding.UTF8.GetString(result);

            User[] userarray = JsonConvert.DeserializeObject<User[]>(responseString);

            UserDialogs.Instance.HideLoading();

            foreach (User user in userarray)
            {
                userList.Add(user);
            }
            return userList;
        }

        public List<User> getUser(int uid)
        {
            List<User> userList = new List<User>();

            UserDialogs.Instance.ShowLoading();

            //fetch data from DB API
            WebClient webClient = new WebClient();

            webClient.QueryString.Add("uid", "" + uid);

            var result = webClient.UploadValues("http://impression-real-estate.co.nf/user/getUser.php", "POST", webClient.QueryString);

            string responseString = UnicodeEncoding.UTF8.GetString(result);

            User[] userarray = JsonConvert.DeserializeObject<User[]>(responseString);

            UserDialogs.Instance.HideLoading();

            foreach (User user in userarray)
            {
                userList.Add(user);
            }
            return userList;
        }
    }
}
