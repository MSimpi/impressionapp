﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionApp.Models
{
    public class PhpMessage
    {
        public string sqlflag { get; set; }
        public string message { get; set; }

        public PhpMessage()
        {

        }
        public PhpMessage(string sqlflag, string message)
        {
            this.sqlflag = sqlflag;
            this.message = message;
        }
    }
}
