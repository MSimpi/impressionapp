﻿using ImpressionApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PCLStorage;  
using ImpressionApp.Views.Pages;
using ImpressionApp.Services;

namespace ImpressionApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        IFolder folder = FileSystem.Current.LocalStorage;

        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        
        public MainPage()
        {
            InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;

            MenuPages.Add((int)MenuItemType.Search, (NavigationPage)Detail);

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            showSplashFeatures();
        }

        async public void showSplashFeatures()
        {

            string firstUser = await PCLHelper.ReadAllTextAsync("firstUser.txt");
            if(firstUser.Equals(""))
            {
                var splashFeature = new SplashFeature();

                NavigationPage navPage = new NavigationPage(splashFeature);

                await Navigation.PushModalAsync(navPage);


                String filename = "firstUser.txt";
                IFolder folder = FileSystem.Current.LocalStorage;
                IFile file = await folder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

                bool saveResult = await PCLHelper.WriteTextAllAsync(filename, "N");
            }

        }

        public async Task NavigateFromMenu(int id)
        {
            if (!MenuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case (int)MenuItemType.Search:
                        MenuPages.Add(id, new NavigationPage(new Listing.SearchPage()));
                        break;
                    case (int)MenuItemType.Profile:
                        MenuPages.Add(id, new NavigationPage(new Auth.ProfilePage()));
                        break;
                    case (int)MenuItemType.Enquiry:
                        MenuPages.Add(id, new NavigationPage(new Pages.EnquiryPage()));
                        break;
                    case (int)MenuItemType.Tsncs:
                        MenuPages.Add(id, new NavigationPage(new Pages.TsncsPage()));
                        break;
                    case (int)MenuItemType.About:
                        MenuPages.Add(id, new NavigationPage(new Pages.AboutPage()));
                        break;
                    case (int)MenuItemType.Feedback:
                        MenuPages.Add(id, new NavigationPage(new Pages.Feedback()));
                        break;
                    default:
                        MenuPages.Add(id, new NavigationPage(new Listing.SearchPage()));
                        break;
                }
            }

            var newPage = MenuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(50);

                IsPresented = false;
            }
        }

    }
}