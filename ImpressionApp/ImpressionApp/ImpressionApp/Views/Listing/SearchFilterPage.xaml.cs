﻿using ImpressionApp.Models;
using ImpressionApp.Services;
using ImpressionApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Listing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchFilterPage : ContentPage
    {
        //SearchFilterViewModel viewModel { get; set; } = GlobalVar.FilterViewModel;
        SearchFilterViewModel viewModel;
        Boolean _flag;

        public SearchFilterPage(Boolean Listing_Flag)
        {
            Title = "Filters";
            InitializeComponent();

            BindingContext = viewModel = GlobalVar.FilterViewModel;
            _flag = Listing_Flag;
            InitFilters();
        }

        public void InitFilters()
        {
            flt09_squ.SetBinding(Picker.ItemsSourceProperty, "SquaObList");
            flt09_squ.ItemDisplayBinding = new Binding("feature_text_value");
            flt09_squ.SetBinding(Picker.SelectedItemProperty, "SquaSelectedItem");

            flt20_car.SetBinding(Picker.ItemsSourceProperty, "CarObList");
            flt20_car.ItemDisplayBinding = new Binding("feature_text_value");
            flt20_car.SetBinding(Picker.SelectedItemProperty, "CarSelectedItem");

            flt21_gar.SetBinding(Picker.ItemsSourceProperty, "GarObList");
            flt21_gar.ItemDisplayBinding = new Binding("feature_text_value");
            flt21_gar.SetBinding(Picker.SelectedItemProperty, "GarSelectedItem");

            flt13_ala.SetBinding(Switch.IsToggledProperty, "BoolAlarm");
            flt14_pool.SetBinding(Switch.IsToggledProperty, "BoolPool");
            flt18_aircon.SetBinding(Switch.IsToggledProperty, "BoolAirConditioning");

            //bindFilter(flt16_ren, "Rent");
            //bindFilter(flt17_yie, "Yiel");

            if (viewModel.BedrObList.Count == 0)
            {
                viewModel.LoadFiltersCommand.Execute(null);
            }

            ////for access part
            //viewModel.LoadFeatureAccessCmd.Execute(null);
            //var list = viewModel.FeatureAccessObList;
            //foreach (Feature_Access item in list)
            //{
            //    switch (item.feature_value)
            //    {
            //        case GlobalVar.FLT09_SQUAREM:
            //            InitFilterAccess(flt09_squ, item);
            //            break;
            //        case GlobalVar.FLT13_ALA:
            //            InitFilterAccess(flt13_ala, item);
            //            break;
            //        case GlobalVar.FLT16_RENT:
            //            InitFilterAccess(flt16_ren, item);
            //            break;
            //        case GlobalVar.FLT20_CAR_PORTS:
            //            InitFilterAccess(flt20_car, item);
            //            break;
            //        case GlobalVar.FLT21_GARAGES:
            //            InitFilterAccess(flt21_gar, item);
            //            break;
            //        case GlobalVar.FLT22_RENT_PER_WEEK:
            //            InitFilterAccess(flt16_ren, item);
            //            break;
            //        default:
            //            return;
            //    }
            //}
        }

        public void bindFilter(Picker picker, string prefix)
        {
            picker.SetBinding(Picker.ItemsSourceProperty, prefix + "ObList");
            picker.SetBinding(Picker.SelectedItemProperty, prefix + "SelectedItem");
            picker.ItemDisplayBinding = new Binding("feature_text_value");
        }

        private async void Click_ApplyFilter(object sender, EventArgs e)
        {
            if (_flag)
            {
                await Navigation.PopAsync(true);
            }
            else
            {
                //await Navigation.PopAsync(true);
                await Navigation.PushAsync(new ItemsPage(), false);
            }
        }

        private void InitFilterAccess(View view, Feature_Access item)
        {
            if ("active" != item.status)
            {
                view.IsEnabled = false;
            }
            else
            {
                view.IsEnabled = true;
            }
        }
    }
}