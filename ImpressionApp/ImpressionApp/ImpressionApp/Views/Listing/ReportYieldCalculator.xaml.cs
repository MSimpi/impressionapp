﻿using System;
using System.Collections.Generic;
using ImpressionApp.Models;
using ImpressionApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Listing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReportYieldCalculator : ContentPage
    {
        ItemDetailViewModel viewModel { get; set; }
        YieldCalculator yieldCalcViewModel { get; set; }
        private ReportYieldMortgView yieldMortgage;
        private ReportYieldInvestView yieldInvestment;

        Page page;
        public ReportYieldCalculator(ItemDetailViewModel vm)
        {

            try
            {
                InitializeComponent();
                txtShower.Text = vm.property.bathrooms;
                yieldCalcViewModel = new YieldCalculator();
                yieldCalcViewModel.purchasePrice = Double.Parse(vm.property.price);
                BindingContext = this.viewModel = vm;

                page = this;
                yieldMortgage = new ReportYieldMortgView(this, yieldCalcViewModel);
                yieldInvestment = new ReportYieldInvestView(this, yieldCalcViewModel);
                SegContent.Children.Add(yieldMortgage);
            }
            catch (Exception ex)
            {
                Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
            }
        }

        void Handle_ValueChanged(object sender, SegmentedControl.FormsPlugin.Abstractions.ValueChangedEventArgs e)
        {
            switch (e.NewValue)
            {
                case 0:
                    SegContent.Children.Clear();
                    //SegContent.Children.Add(new Label() { Text = "Tab 1 selected" });
                    if(yieldMortgage == null)
                    {
                        yieldMortgage = new ReportYieldMortgView(page, yieldCalcViewModel);
                    }

                    SegContent.Children.Add(yieldMortgage);
                    break;
                case 1:
                    SegContent.Children.Clear();
                    if (yieldInvestment == null)
                    {
                        yieldInvestment = new ReportYieldInvestView(page, yieldCalcViewModel);
                    }
                        
                    //SegContent.Children.Add(new Label() { Text = "Tab 2 selected" });
                    SegContent.Children.Add(yieldInvestment);
                    break;
            }
        }

    }
}
