﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using ImpressionApp.Models;
using ImpressionApp.ViewModels;
using System;
using System.Threading;

namespace ImpressionApp.Views.Listing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;

        ItemDetailsInfoView infoView;
        ItemDetailsFeatureView featureView = new ItemDetailsFeatureView();
        ItemDetailsAgentView agentView;

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            try
            {
                InitializeComponent();

                var view = new ItemDetailCarouselView(new ItemDetailSliderViewModel(viewModel));
                ItemDetailMainLayout.Children.Add(view);


                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"(<br />|<br/>|</ br>|</br>)");
                viewModel.property.description = regex.Replace(viewModel.property.description, "\r\n");

                BindingContext = this.viewModel = viewModel;

                infoView = new ItemDetailsInfoView(viewModel);
                agentView = new ItemDetailsAgentView(viewModel);
            }
            catch (Exception ex)
            {
                Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
            }

        }

        void Handle_ValueChanged(object sender, SegmentedControl.FormsPlugin.Abstractions.ValueChangedEventArgs e)
        {
            switch (e.NewValue)
            {
                case 0:
                    SegContent.Children.Clear();
                    SegContent.Children.Add(infoView);
                    break;
                case 1:
                    SegContent.Children.Clear();
                    SegContent.Children.Add(featureView);
                    break;
               /* case 2:
                    SegContent.Children.Clear();
                    SegContent.Children.Add(advancedViewBuyer);
                    SegContent.Children.Add(advancedViewInvestor);
                    break;*/
                case 2:
                    SegContent.Children.Clear();
                    SegContent.Children.Add(agentView);
                    break;
            }
        }

        async void ItemCalc_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ReportYieldCalculator(new ItemDetailViewModel(viewModel.property)));
        }


    }
}