﻿using System;
using System.Collections.Generic;
using ImpressionApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Listing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReportYieldInvestView : ContentView
    {
        Page page;
        YieldCalculator viewModel { get; set; }
        public ReportYieldInvestView(Page page, YieldCalculator vm)
        {
            InitializeComponent();
            this.page = page;
            BindingContext = this.viewModel = vm;
            setPageValue();
        }

        public void setPageValue()
        {
            //Setting Fixed values
            //entryAnnualRates.Text = "1700";
            entryRegionalRates.Text = "150";
            //entryPropertyManagementRate.Text = "8.05";
        }


        public void Calculate_Clicked(Object sender, EventArgs args)
        {
            if (String.IsNullOrEmpty(entryWeeklyRentalIncome.Text))
            {
                page.DisplayAlert("Validator", "Please, enter weekly rental income.", "OK");
            }
            else if (String.IsNullOrEmpty(entryPurchasePrice.Text))
            {
                page.DisplayAlert("Validator", "Please, enter purchase price.", "OK");
            }
            else if (String.IsNullOrEmpty(entryBodyCorpLevy.Text))
            {
                page.DisplayAlert("Validator", "Please, enter body corp levy.", "OK");
            }
            else if (String.IsNullOrEmpty(entryRegionalRates.Text))
            {
                page.DisplayAlert("Validator", "Please, enter regional rates.", "OK");
            }
            /*else if (String.IsNullOrEmpty(entryInsurance.Text))
            {
                page.DisplayAlert("Validator", "Please, enter insurance.", "OK");
            }
            else if (String.IsNullOrEmpty(entryPropertyManagementRate.Text))
            {
                page.DisplayAlert("Validator", "Please, enter property management rate.", "OK");
            }*/
            else
            {

                try
                {
                    double purchasePrice = Double.Parse(entryPurchasePrice.Text);
                    double weeklyRentalIncome = Double.Parse(entryWeeklyRentalIncome.Text);
                    double bodycorpLevy = Double.Parse(entryBodyCorpLevy.Text);
                    double regionalRates = Double.Parse(entryRegionalRates.Text);
                    //double propertyManagementRate = Double.Parse(entryPropertyManagementRate.Text) / 100;
                    //double insurance = Double.Parse(entryInsurance.Text);
                    
                    double totalRates = (bodycorpLevy + regionalRates) / 52;
                    totalRates = Math.Round(totalRates, 2);
                    //entryTotalRates.Text = "$"+ totalRates;

                    /*double pmCost = (weeklyRentalIncome * propertyManagementRate);
                    pmCost = Math.Round(pmCost, 2);
                    entryPmCost.Text = "$" + pmCost;*/

                    double rentExpensesWeekly = (weeklyRentalIncome - totalRates);
                    rentExpensesWeekly = Math.Round(rentExpensesWeekly, 2);
                    entryRentExpensesWeekly.Text = "$" + rentExpensesWeekly;

                    double rentExpensesMonthly = Math.Round((rentExpensesWeekly * 52 / 12), 2);
                    entryRentExpensesMonthly.Text = "$" + rentExpensesMonthly;

                    double grossYield = ((weeklyRentalIncome * 52) / purchasePrice) * 100;
                    grossYield = Math.Round(grossYield, 2);                    
                    entryGrossYield.Text = grossYield + "%";

                    double netYield = (((weeklyRentalIncome * 52) - bodycorpLevy - regionalRates) / purchasePrice) * 100;
                    netYield = Math.Round(netYield, 2);
                    entryNetYield.Text = netYield + "%";

                    entryCashflow.Text = "$" + (rentExpensesWeekly - viewModel.rentExpensesWeekly);

                    /*if (viewModel.rentExpensesWeekly != 0)
                    {
                        entryCashflow.Text = "$" + (rentExpensesWeekly - viewModel.rentExpensesWeekly);
                    }
                    else
                    {
                        entryCashflow.IsVisible = false;
                        labelCashflow.IsVisible = false;
                    }*/
                }
                catch (Exception ex)
                {
                    Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
                }
            }
        }
    }
}
