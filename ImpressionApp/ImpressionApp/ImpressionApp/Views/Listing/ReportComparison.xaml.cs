﻿using ImpressionApp.Models;
using ImpressionApp.ViewModels;
using Sharpnado.Presentation.Forms.RenderedViews;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Listing
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReportComparison : ContentPage
	{
        PropertyListingViewModel viewModel;

        public ReportComparison(PropertyListingViewModel vm)
        {
            InitializeComponent();
            BindingContext = viewModel = vm;
        }

        private void OnRemove(object sender, EventArgs args)
        {
            var button = sender as Button;
            var prop = button.BindingContext as Property;
            viewModel.RemoveComparisonCommand.Execute(prop);

            ////auto go back, but conflict
            //if (viewModel.ListComparison.Count < 2)
            //{
            //    Navigation.PopAsync(true);
            //}
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            //reset the itemssource to avoid "System.ObjectDisposedException"
            CompListView.ItemsSource = new ObservableCollection<Property>();
        }
    }
}