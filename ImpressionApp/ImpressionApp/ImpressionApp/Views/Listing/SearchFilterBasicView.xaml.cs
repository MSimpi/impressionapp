﻿using ImpressionApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Acr.UserDialogs;
using ImpressionApp.Models;
using ImpressionApp.Services;

namespace ImpressionApp.Views.Listing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchFilterBasicView : ContentView
    {
        SearchFilterViewModel viewModel;

        public SearchFilterBasicView()
        {
            InitializeComponent();

            try
            {
                //UserDialogs.Instance.ShowLoading();

                BindingContext = viewModel = GlobalVar.FilterViewModel;

                /*if (viewModel.flt01_Sub_Dict.Count == 0)
                {
                    viewModel.LoadFilterSuburbCommand.Execute(null);
                    InitFilterFromDict(viewModel.flt01_Sub_Dict, Flt01_Suburb);
                }

                if (viewModel.CityObList.Count == 0)
                {
                    viewModel.LoadFilterCityCommand.Execute(null);
                    //InitFilterFromDict(viewModel.flt01_Cit_Dict, Flt01_City);
                    InitFilterFromDict(null, Flt01_City);
                }*/

                if (viewModel.BathObList.Count == 0)
                {
                    viewModel.LoadFiltersCommand.Execute(null);
                    GlobalVar.FilterViewModel = viewModel;
                }

                Flt02_Bedroom.SetBinding(Picker.ItemsSourceProperty, "BedrObList");
                Flt02_Bedroom.ItemDisplayBinding = new Binding("feature_text_value");
                Flt02_Bedroom.SetBinding(Picker.SelectedItemProperty, "BedrSelectedItem");

                Flt03_Bathroom.SetBinding(Picker.ItemsSourceProperty, "BathObList");
                Flt03_Bathroom.ItemDisplayBinding = new Binding("feature_text_value");
                Flt03_Bathroom.SetBinding(Picker.SelectedItemProperty, "BathSelectedItem");
                
                Flt04_Price.SetBinding(Picker.ItemsSourceProperty, "PricObList");
                Flt04_Price.ItemDisplayBinding = new Binding("feature_text_value");
                Flt04_Price.SetBinding(Picker.SelectedItemProperty, "PricSelectedItem");

                Flt05_rent.SetBinding(Picker.ItemsSourceProperty, "RentObList");
                Flt05_rent.ItemDisplayBinding = new Binding("feature_text_value");
                Flt05_rent.SetBinding(Picker.SelectedItemProperty, "RentSelectedItem");

                Flt00_type.Items.Add("Any");
                Flt00_type.Items.Add("House");
                Flt00_type.Items.Add("Apartment");
                Flt00_type.Items.Add("Studio");
                Flt00_type.Items.Add("Unit");
                Flt00_type.Items.Add("Carpark");
                Flt00_type.Items.Add("Townhouse");
                Flt00_type.SetBinding(Picker.SelectedItemProperty, "TypeSelectedItem");
            }
            catch (Exception ex)
            {
                Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
            }
            finally
            {
                //UserDialogs.Instance.HideLoading();
            }
        }

        private void OnReset(object sender, EventArgs args)
        {
            viewModel.ExecuteResetFilters();
            //BindingContext = viewModel = new SearchFilterViewModel();
            //viewModel.LoadFiltersCommand.Execute(null);
            GlobalVar.FilterViewModel = viewModel;
        }
    }
}