﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using ImpressionApp.ViewModels;
using ImpressionApp.Services;
using ImpressionApp.Models;
using System.Diagnostics;

namespace ImpressionApp.Views.Listing
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchPage : ContentPage
    {
        SearchSliderViewModel _svm;

        public SearchPage ()
		{
			InitializeComponent ();

            Title = "Search";

            BindingContext = _svm = new SearchSliderViewModel();
            NavigationPage.SetHasNavigationBar(this, true);

            if (Device.RuntimePlatform == Device.Android)
            {
                searchtext.TextColor = Color.White;
            }
            else if(Device.RuntimePlatform == Device.iOS)
            {
                searchView.Margin = new Thickness(0, 20, 0, 0);
            }
        }
        
        private async void Btn_Search(object sender, EventArgs e)
        {
            if (!CommonFunction.CheckingInternet())
            {
                await DisplayAlert("Notice", "There is no internet connection.", "OK");
            }
            else
            {
                Application.Current.Properties["searchtext"] = searchtext.Text;

                searchtext.Text = "";
                NavigationPage.SetHasNavigationBar(this, true);
                searchView.IsVisible = false;

                await Navigation.PushAsync(new ItemsPage(), false);
            }

        }
        private void Btn_SearchBar(object sender, EventArgs e)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            searchView.IsVisible = true;
            searchtext.Focus();
        }

        private void Btn_HideSearch(object sender, EventArgs e)
        {
            NavigationPage.SetHasNavigationBar(this, true);
            searchView.IsVisible = false;
        }

        private async void Btn_Filter_Advanced(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SearchFilterPage(false), false);
        }

        void Handle_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            Debug.WriteLine("Position " + e.NewValue + " selected.");
        }

        void Handle_Scrolled(object sender, CarouselView.FormsPlugin.Abstractions.ScrolledEventArgs e)
        {
            Debug.WriteLine("Scrolled to " + e.NewValue + " percent.");
            Debug.WriteLine("Direction = " + e.Direction);
        }
    }
}