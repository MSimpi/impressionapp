﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImpressionApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Listing
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemDetailsAgentView : ContentView
	{
		public ItemDetailsAgentView (ItemDetailViewModel vm)
		{
			InitializeComponent ();

            vm.LoadAgentCommand.Execute(null);
            if (vm.agent != null)
            {
                gridAgent.IsVisible = true;
                layoutNoAgent.IsVisible = false;
            }
            else
            {
                gridAgent.IsVisible = false;
                layoutNoAgent.IsVisible = true;
            }
        }

        async public void Send_Enquery(object sender, EventArgs e)
        {
            //Application.Current.Properties["agentName"] = agent_name.Text;
            Application.Current.Properties["agentEmail"] = agent_email.Text;
            await Navigation.PushAsync(new Pages.EnquiryPage(), false);
        }
    }
}