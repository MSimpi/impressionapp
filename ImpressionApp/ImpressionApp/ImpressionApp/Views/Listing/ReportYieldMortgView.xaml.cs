﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Acr.UserDialogs;
using ImpressionApp.Services;
using ImpressionApp.Models;

namespace ImpressionApp.Views.Listing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReportYieldMortgView : ContentView
    {
        Page page;
        YieldCalculator viewModel { get; set; }
        double interestRate = 4.29;
        static double weeklyMortgagePaymnet = 0;

        public ReportYieldMortgView(Page page, YieldCalculator vm)
        {
            InitializeComponent();
            this.page = page;
            BindingContext = this.viewModel = vm;

            entryInterestRate.Text = interestRate + "";
        }

        async void ValidateUser()
        {
            await page.DisplayAlert("Alert from View Model", "", "Ok");
        }

        public void Calculate_Clicked(Object sender, EventArgs args)
        {
            if (String.IsNullOrEmpty(entryPurchasePrice.Text))
            {
                page.DisplayAlert("Validator", "Please, enter purchase price.", "OK");
            }
            else if (String.IsNullOrEmpty(entryDeposit.Text))
            {
                page.DisplayAlert("Validator", "Please, enter deposit price.", "OK");
            }
            else if (String.IsNullOrEmpty(entryLoanAmount.Text))
            {
                page.DisplayAlert("Validator", "Please, enter loan amount.", "OK");
            }
            else if (String.IsNullOrEmpty(entryTerm.Text))
            {
                page.DisplayAlert("Validator", "Please, enter term.", "OK");
            }
            else if (String.IsNullOrEmpty(entryInterestRate.Text))
            {
                page.DisplayAlert("Validator", "Please, enter interest rate.", "OK");
            }
            else
            {

                try
                {
                    int term = int.Parse(entryTerm.Text);
                    double loanAmount = double.Parse(entryLoanAmount.Text);
                    weeklyMortgagePaymnet = PMT(interestRate, term * 52, loanAmount);
                    double monthlyMortgagePaymnet = weeklyMortgagePaymnet * 52 / 12;

                    weeklyMortgagePaymnet = Math.Round(weeklyMortgagePaymnet, 2);
                    monthlyMortgagePaymnet = Math.Round(monthlyMortgagePaymnet, 2);
                    entryWeeklyPayment.Text = "$" + weeklyMortgagePaymnet;
                    entryMonthlyPayment.Text = "$" + monthlyMortgagePaymnet;

                    this.viewModel.rentExpensesWeekly = weeklyMortgagePaymnet;
                }
                catch (Exception ex)
                {
                    Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
                }
            }
        }

        void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            double purchasePrice, deposit, loanAmount;
            if (!String.IsNullOrEmpty(entryPurchasePrice.Text) && !String.IsNullOrEmpty(entryDeposit.Text))
            {
                purchasePrice = Double.Parse(entryPurchasePrice.Text);
                deposit = Double.Parse(entryDeposit.Text);
                loanAmount = purchasePrice - deposit;
                entryLoanAmount.Text = ""+loanAmount;
            }
            //throw new NotImplementedException();
        }

        public static double PMT(double yearlyInterestRate, int totalNumberOfMonths, double loanAmount)
        {
            var rate = (double)yearlyInterestRate / 100 / 52;
            var denominator = Math.Pow((1 + rate), totalNumberOfMonths) - 1;
            return (rate + (rate / denominator)) * loanAmount;
        }

        public static double getWeeklyPayment()
        {
            return weeklyMortgagePaymnet;
        }
    }
}
