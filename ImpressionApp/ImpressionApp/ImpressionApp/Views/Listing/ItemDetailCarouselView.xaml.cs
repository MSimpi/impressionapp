﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ImpressionApp.ViewModels;

namespace ImpressionApp.Views.Listing
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemDetailCarouselView : ContentView
	{
        ItemDetailSliderViewModel viewModel;

        public ItemDetailCarouselView ()
		{
			InitializeComponent ();
            BindingContext = viewModel = new ItemDetailSliderViewModel();
        }

        public ItemDetailCarouselView(ItemDetailSliderViewModel vm)
        {
            InitializeComponent();
            BindingContext = viewModel = vm;
        }

        void Handle_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            Debug.WriteLine("Position " + e.NewValue + " selected.");
        }

        void Handle_Scrolled(object sender, CarouselView.FormsPlugin.Abstractions.ScrolledEventArgs e)
        {
            Debug.WriteLine("Scrolled to " + e.NewValue + " percent.");
            Debug.WriteLine("Direction = " + e.Direction);
        }
    }
}