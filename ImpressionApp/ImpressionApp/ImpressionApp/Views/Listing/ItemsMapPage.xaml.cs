﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;
using Xamarin.Essentials;
using ImpressionApp.Models;
using ImpressionApp.Services;
using ImpressionApp.ViewModels;

namespace ImpressionApp.Views.Listing
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemsMapPage : ContentPage
	{

        public ItemsMapPage(PropertyListingViewModel viewModel)
        {
            InitializeComponent();

            //First location: 365 Queen Street, Auckland City
            map.MoveToRegion(
            MapSpan.FromCenterAndRadius(
                new Position(-36.854722, 174.762351), Distance.FromMiles(1)));

            var list = viewModel.Lists;
            List<Location> locations = new List<Location>();
            foreach (Property Prop in list)
            {
                Location location = new Location(Prop.street_number +", " +Prop.street + ", "+Prop.suburb + ", "+Prop.region, Prop.title, Prop);
                locations.Add(location);
            }

            Task<Position> positionTask = CommonFunction.getCoordinates(locations, map, this);
        }

        public void Marker_Clicked(Object sender, EventArgs args)
        {
            var property = ((CustomPin)sender).property;
            if (property == null)
                return;
            Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(property)));
        }

        public void List_Clicked(Object sender, EventArgs args)
        {
            Navigation.PopModalAsync();
        }
    }
}