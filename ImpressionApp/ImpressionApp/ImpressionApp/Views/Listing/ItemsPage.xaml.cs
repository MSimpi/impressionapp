﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using ImpressionApp.Models;
using ImpressionApp.Views;
using ImpressionApp.ViewModels;
using ImpressionApp.Services;
using System.Windows.Input;
using PCLStorage;
using System.Net.Mail;
using System.Collections.ObjectModel;
using Plugin.Toast;

namespace ImpressionApp.Views.Listing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemsPage : ContentPage
    {
        PropertyListingViewModel viewModel;

        public ItemsPage()
        {
            try
            {
                InitializeComponent();

                BindingContext = viewModel = new PropertyListingViewModel();
            }
            catch (Exception ex)
            {
                Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            CommonFunction.screenLoading(500);

            //if (viewModel.Lists.Count == 0)
                viewModel.LoadPropertyCommand.Execute(null);
        }

        async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            if (!(args.Item is Property property))
                return;

            await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(property)));
        }

        async void ItemMap_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new ItemsMapPage(viewModel)));
        }

        private async void ButtonFilter_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SearchFilterPage(true));
        }

        private async void OnTapYieldCalculator(object sender, EventArgs args)
        {
            var arg = (TappedEventArgs)args;
            var property = (arg.Parameter as Property);
            if (property == null)
                return;

            await Navigation.PushAsync(new ReportYieldCalculator(new ItemDetailViewModel(property)));
        }

        private async void OnTapAddComparisonReport(object sender, EventArgs args)
        {
            //event handler
            var arg = (TappedEventArgs)args;
            var property = (arg.Parameter as Property);
            if (property == null)
                return;

            if (viewModel.ListComparison.Contains(property))
            {
                await DisplayAlert("Comparison Report", property.title + " already added!", "OK");
                return;
            }


            if (viewModel.ListComparison.Count > 5)
            {
                CrossToastPopUp.Current.ShowToastError("You can add up to 5 Properties!");
                return;
            }


            //add to view model
            viewModel.ListComparison.Add(property);

            if (viewModel.ListComparison.Count > 2)
            {
                bool answer = await DisplayAlert("Comparison Report", property.title + " added successfully!", "View Report", "OK");
                if (answer)
                {
                    await Navigation.PushAsync(new ReportComparison(viewModel));
                }
            }
            else
            {
                await DisplayAlert("Comparison Report", property.title + " added successfully! ", "OK");
            }
        }

        private async void OnBtnComparisonReport(object sender, EventArgs args)
        {
            if (viewModel.ListComparison.Count < 3)
            {
                //add more to compare
                CrossToastPopUp.Current.ShowToastError("Please add properties in comparison!");
                //await DisplayAlert("Note", "Please add more properties !", "OK");
            }
            else
            {
                await Navigation.PushAsync(new ReportComparison(viewModel));
            }
        }
    }
}