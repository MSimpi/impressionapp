﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ImpressionApp.Services;
using Xamarin.Forms.Maps;
using ImpressionApp.ViewModels;

namespace ImpressionApp.Views.Listing
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemDetailsInfoView : ContentView
	{
        ItemDetailViewModel viewModel { get; set; }


        public ItemDetailsInfoView(ItemDetailViewModel vm)
        {

            try
            {
                InitializeComponent();

                this.viewModel = vm;

                Location location = new Location(vm.property.street_number +", "+ vm.property.street + ", " + vm.property.suburb + ", " + vm.property.region, vm.property.title, vm.property);


                List<Location> locations = new List<Location>();
                locations.Add(location);

                Task<Position> positionTask = CommonFunction.getCoordinates(locations, map, null);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //await Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
            }
        }
    }
}