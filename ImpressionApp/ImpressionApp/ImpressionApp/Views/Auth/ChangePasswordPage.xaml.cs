﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Acr.UserDialogs;
using ImpressionApp.Models;
using ImpressionApp.Services;
using Xamarin.Forms;

namespace ImpressionApp.Views.Auth
{
    public partial class ChangePasswordPage : ContentPage
    {
        public string email;
        public string userid;

        public ChangePasswordPage()
        {
            InitializeComponent();
        }

        async public void ChangePassword_Clicked(Object sender, EventArgs args)
        {
            if (String.IsNullOrEmpty(txtCurrentPw.Text))
            {
                await DisplayAlert("Validator", "Please, enter current password.", "OK");
            }
            else if (String.IsNullOrEmpty(txtNewPw.Text))
            {
                await DisplayAlert("Validator", "Please, enter new password.", "OK");
            }
            else if (txtCurrentPw.Text == txtNewPw.Text)
            {
                await DisplayAlert("Validator", "Please, enter different password as before.", "OK");
            }
            else if (String.IsNullOrEmpty(txtConfirmPw.Text))
            {
                await DisplayAlert("Validator", "Please, enter confirm password.", "OK");
            }
            else if (txtNewPw.Text != txtConfirmPw.Text)
            {
                await DisplayAlert("Validator", "New Password and Confirm Password should be the same", "OK");
            }
            else
            {

                try
                {
                    using (UserDialogs.Instance.Loading())
                    {
                        await Task.Delay(3000);

                        userid = Application.Current.Properties["UserId"].ToString();
                        email = Application.Current.Properties["UserEmail"].ToString();

                        //Hashing password
                        string hashingCurPw = CommonFunction.GetHash256(txtCurrentPw.Text, email);
                        string hashingNewPw = CommonFunction.GetHash256(txtNewPw.Text, email);

                        List<User> userList = new User().loginUser(email, hashingCurPw);

                        foreach (var item in userList)
                        {
                            if (item != null)
                            {
                                PhpMessage msg = new User().updateUserPassword(int.Parse(userid), hashingCurPw, hashingNewPw);

                                await DisplayAlert("Notice", "Your password updated successfully", "Ok");
                                Application.Current.MainPage = new MainPage();
                            }
                            else
                            {
                                await DisplayAlert("Oops!", "Invalid email or password!", "Ok");
                            }
                        }
                    }

                }
                catch (Exception)
                {
                    await DisplayAlert("Oops!", "Invalid email or password!", "Ok");
                    //Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
