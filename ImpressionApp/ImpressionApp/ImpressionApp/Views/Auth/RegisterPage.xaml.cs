﻿using ImpressionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImpressionApp.Services;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Acr.UserDialogs;
using Xamarin.Forms.Maps;

namespace ImpressionApp.Views.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        //private int type = 1;
        string suburb = "", postcode = "", address = "";

        public RegisterPage()
        {
            InitializeComponent();

            switchCurrentLocation.Toggled += HandleSwitchToggled;

        }

        public void PrivacyPolicy_Clicked(Object sender, EventArgs args)
        {
            var privacyPolicy = new Pages.PrivacyPolicy();

            privacyPolicy.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });
            NavigationPage navPage = new NavigationPage(privacyPolicy);

            Navigation.PushModalAsync(navPage);
        }

        /*
        public void BuyToRent_Clicked(Object sender, EventArgs args)
        {
            Resources["OwnerBtnStyle"] = Resources["btnRedStyle"];
            Resources["InvestorBtnStyle"] = Resources["btnWhiteStyle"];
            type = 1;
        }

        public void BuyToOwn_Clicked(Object sender, EventArgs args)
        {
            Resources["InvestorBtnStyle"] = Resources["btnRedStyle"];
            Resources["OwnerBtnStyle"] = Resources["btnWhiteStyle"];
            type = 2;
        }
        */

        async public void SignUp_Clicked(Object sender, EventArgs args)
        {
            try
            {
                if (String.IsNullOrEmpty(txtFirstName.Text))
                {
                    await DisplayAlert("Validator", "Please, enter first name.", "OK");
                }
                else if (String.IsNullOrEmpty(txtLastName.Text))
                {
                    await DisplayAlert("Validator", "Please, enter last name.", "OK");
                }
                else if (String.IsNullOrEmpty(userId.Text))
                {
                    await DisplayAlert("Validator", "Please, enter user ID.", "OK");
                }
                else if (String.IsNullOrEmpty(txtUserPw.Text))
                {
                    await DisplayAlert("Validator", "Please, enter password.", "OK");
                }
                else if (String.IsNullOrEmpty(txtUserPwConfirm.Text))
                {
                    await DisplayAlert("Validator", "Please, enter confirm password.", "OK");
                }
                else if (txtUserPw.Text != txtUserPwConfirm.Text)
                {
                    await DisplayAlert("Validator", "Password and confirm password should be the same.", "OK");
                }
                else
                {
                    using (UserDialogs.Instance.Loading("Signing up..."))
                    {
                        await Task.Delay(3000);

                        // Save location information
                        if (switchCurrentLocation.IsToggled)
                        {
                            //get currunt location
                            CommonFunction.checkLocationPermission(this, null);
                        }
                        else
                        {
                            //Hashing password
                            string hashingPw = CommonFunction.GetHash256(txtUserPw.Text, userId.Text);

                            PhpMessage msg = new User().createUser(txtFirstName.Text, txtLastName.Text, userId.Text, hashingPw, suburb, postcode, address);

                            UserDialogs.Instance.HideLoading();

                            if (msg.sqlflag == "true")
                            {
                                await DisplayAlert("Wow!", msg.message, "Ok");
                                await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                            }
                            else
                            {
                                await Navigation.PushAsync(new Pages.ErrorPage(msg.message));
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                await Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
            }
        }

        public void HandleSwitchToggled(object sender, ToggledEventArgs e)
        {
            if (switchCurrentLocation.IsToggled)
            {
                //pickerCountry.IsVisible = false;
            }
            else
            {
                // pickerCountry.IsVisible = true;
            }
        }

        public void permissionResult(string result)
        {
            if (result.Equals("true"))
            {
                Task<Position> positionTask = CommonFunction.getUserLocation();
                Position position = positionTask.Result;

                Task<Location> locationTask = CommonFunction.getAddress(position, this, null);
            }
            else
            {
                DisplayAlert("Nofitication", result, "OK");
            }
        }

        public void submitRegister(Location location)
        {

            suburb = location.Suburb;
            postcode = location.Postcode;
            address = location.Address;

            //Hashing password
            string hashingPw = CommonFunction.GetHash256(txtUserPw.Text, userId.Text);

            PhpMessage msg = new User().createUser(txtFirstName.Text, txtLastName.Text, userId.Text, hashingPw, suburb, postcode, address);

            UserDialogs.Instance.HideLoading();

            if (msg.sqlflag == "true")
            {
                DisplayAlert("Wow!", msg.message, "Ok");
                Navigation.PushModalAsync(new NavigationPage(new LoginPage()));

            }
            else
            {
                DisplayAlert("Validator", msg.message, "OK");
                //Navigation.PushAsync(new Pages.ErrorPage(msg.message));
            }
        }


    }
}