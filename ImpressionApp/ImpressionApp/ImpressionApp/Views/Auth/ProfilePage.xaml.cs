﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ImpressionApp.Services;
using Acr.UserDialogs;
using ImpressionApp.Models;
using Xamarin.Forms.Maps;

namespace ImpressionApp.Views.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public int userid;
        string suburb = "", postcode = "", address = "";

        public ProfilePage()
        {
            InitializeComponent();

            getProfile();

            //switchCurrentLocation.Toggled += HandleSwitchToggled;
        }


        public void PrivacyPolicy_Clicked(Object sender, EventArgs args)
        {
            var privacyPolicy = new Pages.PrivacyPolicy();

            privacyPolicy.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });
            NavigationPage navPage = new NavigationPage(privacyPolicy);

            Navigation.PushModalAsync(navPage);
        }

        /*
        public void Owner_Clicked(Object sender, EventArgs args)
        {
            Resources["OwnerBtnStyle"] = Resources["btnRedStyle"];
            Resources["InvestorBtnStyle"] = Resources["btnWhiteStyle"];


            //ownerOption.IsVisible = true;
            //investorOption.IsVisible = false;
           
        }

        public void Investor_Clicked(Object sender, EventArgs args)
        {
            Resources["InvestorBtnStyle"] = Resources["btnRedStyle"];
            Resources["OwnerBtnStyle"] = Resources["btnWhiteStyle"];
            //ownerOption.IsVisible = false;
            //investorOption.IsVisible = true;
        }


        public void HandleSwitchToggled(object sender, ToggledEventArgs e)
        {
            if (switchCurrentLocation.IsToggled)
            {
                pickerCountry.IsVisible = false;
            }
            else
            {
                pickerCountry.IsVisible = true;
            }
        }
        */

        public void ChangePassword_Clicked(Object sender, EventArgs args)
        {
            var changePasswordPage = new ChangePasswordPage();

            changePasswordPage.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });
            NavigationPage navPage = new NavigationPage(changePasswordPage);

            Navigation.PushModalAsync(navPage);
        }

        public void Save_Clicked(Object sender, EventArgs args)
        {
            if (CommonFunction.checkGlobalExist("IsLoggedIn") == false)
            {
                DisplayAlert("Validator", "Please, login.", "OK");
                return;
            }

            if (String.IsNullOrEmpty(txtFirstName.Text))
            {
                DisplayAlert("Validator", "Please, enter first name.", "OK");
            }
            else if (String.IsNullOrEmpty(txtLastName.Text))
            {
                DisplayAlert("Validator", "Please, enter last name.", "OK");
            }
            else if (String.IsNullOrEmpty(txtEmail.Text))
            {
                DisplayAlert("Validator", "Please, enter email address.", "OK");
            }
            else if (!CommonFunction.ValidateEmail(txtEmail.Text))
            {
                DisplayAlert("Validator", "Please enter the correct email address.", "OK");
            }
            else if (String.IsNullOrEmpty(txtPhone.Text))
            {
                DisplayAlert("Validator", "Please, enter phone number.", "OK");
            }
            else if (!CommonFunction.IsCorrectMobileNumber(txtPhone.Text))
            {
                DisplayAlert("Validator", "Please enter the correct phone number.", "OK");
            }
            /*
            else if (!switchCurrentLocation.IsToggled && pickerCountry.SelectedItem == null)
            {
                DisplayAlert("Validator", "Please, select Country.", "OK");
            }
            else if (pickerExpiration.IsVisible && pickerExpiration.SelectedItem == null)
            {
                DisplayAlert("Validator", "Please, select expiration.", "OK");
            }
            else if (pickerTimeFrame.IsVisible && pickerTimeFrame.SelectedItem == null)
            {
                DisplayAlert("Validator", "Please, select time frame.", "OK");
            }
            else if (investorOption.IsVisible && pickerInvestPrefer.SelectedItem == null)
            {
                DisplayAlert("Validator", "Please, select invest preference.", "OK");
            }
            */
            else
            {
                try
                {
                    if (switchCurrentLocation.IsToggled)
                    {
                        //get currunt location
                        CommonFunction.checkLocationPermission(null, this);
                    }
                    else
                    {
                        PhpMessage msg = new User().updateUser(userid, txtFirstName.Text, txtLastName.Text, txtEmail.Text, txtPhone.Text, suburb, postcode, address);

                        DisplayAlert("Wow!", "Profile Updated Successfully", "Ok");
                        Application.Current.MainPage = new MainPage();
                    }


                }
                catch (Exception ex)
                {
                    Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
                }
                finally
                {
                    UserDialogs.Instance.HideLoading();
                }

            }
        }

        public void permissionResult(string result)
        {
            if (result.Equals("true"))
            {
                Task<Position> positionTask = CommonFunction.getUserLocation();
                Position position = positionTask.Result;

                Task<Location> locationTask = CommonFunction.getAddress(position, null, this);
            }
            else
            {
                DisplayAlert("Nofitication", result, "OK");
            }
        }

        private void getProfile()
        {
            if (CommonFunction.checkGlobalExist("IsLoggedIn") == true)
            {
                userid = Convert.ToInt32(Application.Current.Properties["UserId"].ToString());

                List<User> userList = new User().getUser(userid);

                foreach (var item in userList)
                {
                    if (item != null)
                    {
                        txtFirstName.Text = item.first_name;
                        txtLastName.Text = item.last_name;
                        txtEmail.Text = item.email;
                        txtPhone.Text = item.phone;
                    }
                }
            }
            else
            {
                DisplayAlert("Validator", "Please, login.", "OK");

                var loginPage = new Auth.LoginPage();

                loginPage.ToolbarItems.Add(new ToolbarItem
                {
                    Text = "Cancel",
                    Command = new Command(() => Navigation.PopModalAsync())
                });

                NavigationPage navPage = new NavigationPage(loginPage);

                Navigation.PushModalAsync(navPage);
            }
        }

        public void submitProfile(Location location)
        {

            suburb = location.Suburb;
            postcode = location.Postcode;
            address = location.Address;

            PhpMessage msg = new User().updateUser(userid, txtFirstName.Text, txtLastName.Text, txtEmail.Text, txtPhone.Text, suburb, postcode, address);

            if (msg.sqlflag == "true")
            {
                DisplayAlert("Wow!", "Profile Updated Successfully", "Ok");
                Application.Current.MainPage = new MainPage();
            }
            else
            {
                Navigation.PushAsync(new Pages.ErrorPage(msg.message));
            }
        }

    }
}