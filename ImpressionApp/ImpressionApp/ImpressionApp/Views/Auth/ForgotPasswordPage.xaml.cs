﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using ImpressionApp.Services;
using System.Threading.Tasks;
using Xamarin.Essentials;
using ImpressionApp.Models;
using Acr.UserDialogs;

namespace ImpressionApp.Views.Auth
{
    public partial class ForgotPasswordPage : ContentPage
    {
        public ForgotPasswordPage()
        {
            InitializeComponent();
        }

        async public void SendNewPassword_Clicked(Object sender, EventArgs args)
        {
            if (CommonFunction.ValidateEmail(txtRegisterdEmail.Text))
            {
                var randomPassword = CommonFunction.CreateRandomPassword();
                var hashingRandromPassword = CommonFunction.GetHash256(randomPassword, txtRegisterdEmail.Text);

                //await DisplayAlert("Passwords", randomPassword + " \n " + hashingRandromPassword, "OK");

                using (UserDialogs.Instance.Loading())
                {
                    await Task.Delay(7000);

                    PhpMessage msg = new User().updateUserForgotPassword(txtRegisterdEmail.Text, hashingRandromPassword);

                    if (msg.sqlflag == "true")
                    {
                        CommonFunction.sendForgotPasswordEmail(txtRegisterdEmail.Text, randomPassword);
                        await DisplayAlert("Email Sent", "Please check your email!", "OK");
                        await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                    }
                    else
                    {
                        await DisplayAlert("Failed", "" + msg.message, "OK");
                    }
                }
            }
            else
            {
                await DisplayAlert("Validator", "Please enter the correct email address.", "OK");
            }
        }
    }
}
