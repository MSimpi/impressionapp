﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ImpressionApp.Services;
using ImpressionApp.Models;
using Acr.UserDialogs;

namespace ImpressionApp.Views.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }


        public void Signup_Clicked(Object sender, EventArgs args)
        {
            var registerPage = new RegisterPage();

            registerPage.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });
            NavigationPage navPage = new NavigationPage(registerPage);

            Navigation.PushModalAsync(navPage);
        }

        public void ForgotPassword_Clicked(Object sender, EventArgs args)
        {
            var forgotPasswordPage = new ForgotPasswordPage();

            forgotPasswordPage.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });
            NavigationPage navPage = new NavigationPage(forgotPasswordPage);

            Navigation.PushModalAsync(navPage);
        }

        public void PrivacyPolicy_Clicked(Object sender, EventArgs args)
        {
            var privacyPolicy = new Pages.PrivacyPolicy();

            privacyPolicy.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });
            NavigationPage navPage = new NavigationPage(privacyPolicy);

            Navigation.PushModalAsync(navPage);
        }

        async void Login_clicked(Object sender, EventArgs args)
        {
            if (String.IsNullOrEmpty(userId.Text))
            {
                await DisplayAlert("Validator", "Please, enter user ID.", "OK");
            }
            else if (String.IsNullOrEmpty(txtUserPw.Text))
            {
                await DisplayAlert("Validator", "Please, enter password.", "OK");
            }
            else
            {

                try
                {

                    using (UserDialogs.Instance.Loading("Logging In..."))
                    {
                        await Task.Delay(5000);

                        //Hashing password
                        string hashingPw = CommonFunction.GetHash256(txtUserPw.Text, userId.Text);

                        List<User> userList = new User().loginUser(userId.Text, hashingPw);

                        foreach (var item in userList)
                        {
                            if (item != null)
                            {
                                Application.Current.Properties["IsLoggedIn"] = true;
                                Application.Current.Properties["UserId"] = item.user_id;
                                Application.Current.Properties["UserName"] = item.first_name;
                                Application.Current.Properties["UserGroup"] = item.feature_id;
                                Application.Current.Properties["UserEmail"] = item.email;


                                //await DisplayAlert("Wow!", "Login Successfull.", "Ok");
                                //Navigation.PushModalAsync(new MainPage());
                                if (Device.RuntimePlatform == Device.iOS)
                                {
                                    await Navigation.PopToRootAsync();
                                }
                                Application.Current.MainPage = new MainPage();
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    await DisplayAlert("Oops!", "Invalid email or password!", "Ok");
                    //Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}