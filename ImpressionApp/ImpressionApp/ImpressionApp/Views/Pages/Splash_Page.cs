﻿using ImpressionApp.Views;
using ImpressionApp.Views.Pages;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ImpressionApp.Pages
{
    public class Splash_Page : ContentPage
    {
        Image splashImage;

        public Splash_Page()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            var sub = new AbsoluteLayout()
            {
                BackgroundColor = Color.Red
            };
            var stacklayout = new StackLayout()
            {
                BackgroundColor = Color.Blue
            };
            var relativelyt = new RelativeLayout();
            

            splashImage = new Image
            {
                Source = "Splash.png",
                Aspect = Aspect.AspectFill
            };

            AbsoluteLayout.SetLayoutFlags(splashImage, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(splashImage, new Rectangle(0, 0, 1, 1));

            sub.Children.Add(splashImage);
            this.Content = sub;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            //waiting for 3 seconds
            //await Task.Delay(300);
            await Task.Delay(50);

            //jump to the main page
            Application.Current.MainPage = new MainPage();
        }
    }
}
