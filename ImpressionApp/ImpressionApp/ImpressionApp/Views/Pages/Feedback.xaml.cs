﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ImpressionApp.Services;
using Acr.UserDialogs;
using ImpressionApp.Models;

namespace ImpressionApp.Views.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Feedback : ContentPage
    {
        public int userid;

        public Feedback()
        {
            InitializeComponent();
            getProfile();

            /*
            UserDialogs.Instance.ShowLoading();
            System.Threading.Tasks.Task.Factory.StartNew(async () =>
            {
                await System.Threading.Tasks.Task.Delay(3 * 1000);
                UserDialogs.Instance.HideLoading();
            });
            */

        }

        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            double value = args.NewValue;
            string satisfied = String.Format("The Slider value is {0}", value);
        }

        private void getProfile()
        {
            if (CommonFunction.checkGlobalExist("IsLoggedIn") == true)
            {
                userid = Convert.ToInt32(Application.Current.Properties["UserId"].ToString());

                List<User> userList = new User().getUser(userid);

                foreach (var item in userList)
                {
                    if (item != null)
                    {
                        txtFirstName.Text = item.first_name;
                        txtLastName.Text = item.last_name;
                        txtEmail.Text = item.email;
                        txtPhone.Text = item.phone;
                    }
                }
            }
        }


        async public void Send_Clicked(Object sender, EventArgs args)
        {


            if (String.IsNullOrEmpty(txtFirstName.Text))
            {
                await DisplayAlert("Validator", "Please, enter first name.", "OK");
            }
            else if (String.IsNullOrEmpty(txtLastName.Text))
            {
                await DisplayAlert("Validator", "Please, enter last name.", "OK");
            }
            else if (String.IsNullOrEmpty(txtEmail.Text))
            {
                await DisplayAlert("Validator", "Please, enter email address.", "OK");
            }
            else if (!CommonFunction.ValidateEmail(txtEmail.Text))
            {
                await DisplayAlert("Validator", "Please enter the correct email address.", "OK");
            }
            else if (String.IsNullOrEmpty(txtPhone.Text))
            {
                await DisplayAlert("Validator", "Please, enter phone number.", "OK");
            }
            else if (pickerQuery.SelectedItem == null)
            {
                await DisplayAlert("Validator", "Please, select feedback type.", "OK");
            }
            else if (String.IsNullOrEmpty(txtMessage.Text))
            {
                await DisplayAlert("Validator", "Please, enter feedback message.", "OK");
            }
            else
            {
                using (UserDialogs.Instance.Loading())
                {
                    await Task.Delay(3000);

                    if (CommonFunction.sendFeedbackEmail(txtFirstName.Text, txtLastName.Text, txtEmail.Text.Trim(), txtPhone.Text, pickerQuery.SelectedItem.ToString(), slderSatisfied.Value, txtMessage.Text))
                    {
                        await DisplayAlert("Feedback", "Thank you for your feedback!", "OK");
                        Application.Current.MainPage = new MainPage();
                    }
                    else
                    {
                        await DisplayAlert("Send Faild", "Something went wrong!", "OK");
                    }
                }
            }

        }
    }
}