﻿using System;
using System.Collections.Generic;
using ImpressionApp.Services;

using Xamarin.Forms;
using Acr.UserDialogs;
using System.Threading.Tasks;

namespace ImpressionApp.Views.Pages
{
    public partial class SplashFeature : CarouselPage
    {
        CarouselPage carouselPage;
        public SplashFeature()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            carouselPage = this;

            BtnNext.Clicked += (sender, args) =>
            {
                carouselPage.CurrentPage = carouselPage.Children[1];
            };

            BtnPrev.Clicked += (sender, args) =>
            {
                carouselPage.CurrentPage = carouselPage.Children[0];
            };

        }


        public void Close_Clicked(Object sender, EventArgs args)
        {
            Navigation.PopModalAsync();
        }


    }
}
