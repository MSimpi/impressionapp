﻿using ImpressionApp.Models;
using ImpressionApp.Services;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public StackLayout _btnStackLayout;

        public string username { get; set; }



        public MenuPage()
        {
            InitializeComponent();


            if (CommonFunction.checkGlobalExist("IsLoggedIn") == true)
            {
                username = Application.Current.Properties["UserName"].ToString();
                lblusername.Text = "Welcome " + username;

                LoginButtonLayout.IsVisible = false;
                LogoutButtonLayout.IsVisible = true;
            }

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.Search, Title="Search", Image="menu_icon_search.png" },
                new HomeMenuItem {Id = MenuItemType.Profile, Title="Profile", Image="menu_icon_profile.png" },
                new HomeMenuItem {Id = MenuItemType.Enquiry, Title="Enquiry", Image="menu_icon_enquiry.png" },
                new HomeMenuItem {Id = MenuItemType.About, Title="About us", Image="menu_icon_aboutus.png" },
                new HomeMenuItem {Id = MenuItemType.Tsncs, Title="Terms and Conditions", Image="menu_icon_term.png" },
                new HomeMenuItem {Id = MenuItemType.Feedback, Title="Feedback", Image="menu_icon_feedback.png" }
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;

                var title = ((HomeMenuItem)e.SelectedItem).Title;

                if (title == "Enquiry")
                {
                    Application.Current.Properties["agentEmail"] = "";
                }

                if (!CommonFunction.CheckingInternet())
                {
                    await DisplayAlert("Notice", "There is no internet connection.", "OK");
                }
                else
                {
                    await RootPage.NavigateFromMenu(id);
                }
            };



        }


        public void Login_Clicked(Object sender, EventArgs args)
        {
            var loginPage = new Auth.LoginPage();

            loginPage.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });

            if (!CommonFunction.CheckingInternet())
            {
                DisplayAlert("Notice", "There is no internet connection.", "OK");
            }
            else
            {
                NavigationPage navPage = new NavigationPage(loginPage);

                Navigation.PushModalAsync(navPage);
            }
        }

        public void Logout_Clicked(Object sender, EventArgs args)
        {
            if (CommonFunction.Logout() == true)
            {
                //DisplayAlert(" ", "You Are Logout..", "Ok");
                //Navigation.PushModalAsync(new MainPage());
                Application.Current.MainPage = new MainPage();
            }
        }

        public void IconInfo_Clicked(Object sender, EventArgs args)
        {
            var aboutAppPage = new AboutAppPage();

            aboutAppPage.ToolbarItems.Add(new ToolbarItem
            {
                Text = "Cancel",
                Command = new Command(() => Navigation.PopModalAsync())
            });

            if (!CommonFunction.CheckingInternet())
            {
                DisplayAlert("Notice", "There is no internet connection.", "OK");
            }
            else
            {
                NavigationPage navPage = new NavigationPage(aboutAppPage);

                Navigation.PushModalAsync(navPage);
            }
        }
    }
}