﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ImpressionApp.Views.Pages
{
    public partial class ErrorPage : ContentPage
    {
        public ErrorPage(string errorMsg)
        {
            InitializeComponent();
            errorMessage.Text = errorMsg;
        }



        public async void Home_Clicked(Object sender, EventArgs args)
        {

            if (Device.RuntimePlatform == Device.iOS)
            {
                await Navigation.PopToRootAsync();
            }
            Application.Current.MainPage = new MainPage();
        }

    }
}
