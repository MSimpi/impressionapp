﻿using System;
using ImpressionApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Pages
{
    public interface IBaseUrl { string Get(); }

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AboutPage : ContentPage
	{
		public AboutPage ()
		{
			InitializeComponent ();

            var browser = new WebView();
            var htmlSource = new HtmlWebViewSource();
           
            htmlSource.BaseUrl = DependencyService.Get<IBaseUrl>().Get();
            browser.Source = new UrlWebViewSource { Url = System.IO.Path.Combine(htmlSource.BaseUrl, "aboutus.html") }; 
            Content = browser;

            /*
            WebView webView = new WebView
            {
                Source = new UrlWebViewSource
                {
                    Url = "https://impression.co.nz/best-real-estate-agent-auckland/about",
                },
                VerticalOptions = LayoutOptions.FillAndExpand,
                
            };

            this.Content = new StackLayout
            {
                Children =
                {
                    webView
                }
            };*/
        }
    }
}