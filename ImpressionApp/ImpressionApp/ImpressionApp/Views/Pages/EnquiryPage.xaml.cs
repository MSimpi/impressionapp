﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ImpressionApp.Services;
using Xamarin.Essentials;
using Acr.UserDialogs;
using ImpressionApp.Models;
using System.Net.Mail;

namespace ImpressionApp.Views.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnquiryPage : ContentPage
    {
        public EnquiryPage()
        {
            InitializeComponent();
            getProfile();
        }

        private void getProfile()
        {
            if (CommonFunction.checkGlobalExist("IsLoggedIn") == true)
            {
                int userid = Convert.ToInt32(Application.Current.Properties["UserId"].ToString());

                List<User> userList = new User().getUser(userid);

                foreach (var item in userList)
                {
                    if (item != null)
                    {
                        txtFirstName.Text = item.first_name;
                        txtLastName.Text = item.last_name;
                        txtEmail.Text = item.email;
                        txtTelephone.Text = item.phone;
                    }
                }
            }
        }
        async public void Send_Clicked(Object sender, EventArgs args)
        {
            if (!CommonFunction.CheckingInternet())
            {
                await DisplayAlert("Notice", "There is no internet connection.", "OK");
            }
            else
            {
                if (String.IsNullOrEmpty(txtFirstName.Text))
                {
                    await DisplayAlert("Validator", "Please, enter first name.", "OK");
                }
                else if (String.IsNullOrEmpty(txtLastName.Text))
                {
                    await DisplayAlert("Validator", "Please, enter last name.", "OK");
                }
                else if (String.IsNullOrEmpty(txtEmail.Text))
                {
                    await DisplayAlert("Validator", "Please, enter email address.", "OK");
                }
                else if (!CommonFunction.ValidateEmail(txtEmail.Text))
                {
                    await DisplayAlert("Validator", "Please enter the correct email address.", "OK");
                }
                else if (String.IsNullOrEmpty(txtTelephone.Text))
                {
                    await DisplayAlert("Validator", "Please, enter phone number.", "OK");
                }
                else if (String.IsNullOrEmpty(txtDescription.Text))
                {
                    await DisplayAlert("Validator", "Please, enter description.", "OK");
                }
                else
                {
                    using (UserDialogs.Instance.Loading())
                    {
                        await Task.Delay(3000);

                        string agentEmail = Application.Current.Properties["agentEmail"].ToString();
                        if (CommonFunction.sendEnquiryEmail(txtFirstName.Text, txtLastName.Text, txtEmail.Text.Trim(), txtTelephone.Text, txtDescription.Text, agentEmail))
                        {
                            await DisplayAlert("Thank You", "We will contact you soon!", "OK");
                            Application.Current.MainPage = new MainPage();
                        }
                        else
                        {
                            await DisplayAlert("Send Faild", "Something went wrong!", "OK");
                        }
                    }
                }
            }
        }
    }
}