﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ImpressionApp.Views.Pages
{
    public partial class PrivacyPolicy : ContentPage
    {
        public PrivacyPolicy()
        {
            InitializeComponent();

            var browser = new WebView();
            var htmlSource = new HtmlWebViewSource();

            htmlSource.BaseUrl = DependencyService.Get<IBaseUrl>().Get();
            browser.Source = new UrlWebViewSource { Url = System.IO.Path.Combine(htmlSource.BaseUrl, "privacy_policy.html") };
            Content = browser;
        }
    }
}
