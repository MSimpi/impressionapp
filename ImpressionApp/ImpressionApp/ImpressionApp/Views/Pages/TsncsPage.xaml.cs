﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImpressionApp.Views.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TsncsPage : ContentPage
	{
		public TsncsPage ()
		{
			InitializeComponent ();

            var browser = new WebView();
            var htmlSource = new HtmlWebViewSource();

            htmlSource.BaseUrl = DependencyService.Get<IBaseUrl>().Get();
            browser.Source = new UrlWebViewSource { Url = System.IO.Path.Combine(htmlSource.BaseUrl, "tems.html") };
            Content = browser;
        }
	}
}