﻿using FFImageLoading.Forms;
using ImpressionApp.Models;
using ImpressionApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace ImpressionApp.ViewModels
{
    public class ItemDetailSliderViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;
        ItemDetailViewModel viewModel1;
        public ItemDetailSliderViewModel()
        {

        }

        public ItemDetailSliderViewModel(ItemDetailViewModel viewModel1)
        {
            ImageSource = new ObservableCollection<View>();

            this.viewModel1 = viewModel1;
            string imagesJsonString = viewModel1.property.images;

            ImagePath[] imageArray = JsonConvert.DeserializeObject<ImagePath[]>(imagesJsonString);

            foreach (ImagePath imgarray in imageArray)
            {
                if (imgarray.image != "")
                {
                    imgarray.image = "http://impression.co.nz/" + imgarray.image + "_lg.jpg";

                    /* try
                     {
                         HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(imgarray.image);
                         using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                         {
                             //exist = response.StatusCode == HttpStatusCode.OK;
                         }
                     }
                     catch
                     {
                         imgarray.image = "no_image.jpg";
                     }*/

                    CachedImage cachedImage = new CachedImage()
                    {
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.Center,
                        WidthRequest = 300,
                        HeightRequest = 300,
                        CacheDuration = TimeSpan.FromDays(30),
                        DownsampleToViewSize = true,
                        RetryCount = 0,
                        RetryDelay = 250,
                        BitmapOptimizations = false,
                        LoadingPlaceholder = "loading.png",
                        ErrorPlaceholder = "no_image.jpg",
                        Aspect = Aspect.AspectFill,
                        Source = imgarray.image
                    };

                    //Image img = new Image() { Source = imgarray.image, Aspect = Aspect.AspectFill };
                    ImageSource.Add(cachedImage);
                }
            }




            ImageCommand = new Command(() =>
            {
                Debug.WriteLine("Position selected.");
            });

        }
        //}

        ObservableCollection<View> _imageSource;


        public ObservableCollection<View> ImageSource
        {
            set
            {
                _imageSource = value;
                OnPropertyChanged("ImageSource");
            }
            get
            {
                return _imageSource;
            }
        }

        public Command ImageCommand { protected set; get; }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
