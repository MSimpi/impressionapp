﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using ImpressionApp.Models;
using Xamarin.Forms;

namespace ImpressionApp.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Property property { get; set; }
        public Agent agent { get; set; }
        public Command LoadAgentCommand { get; set; }
        public ItemDetailViewModel(Property prop = null)
        {
            Title = property?.title;
            property = prop;
            LoadAgentCommand = new Command(async () => await ExecuteLoadAgent());
        }

        async Task ExecuteLoadAgent()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {
                int agentId = int.Parse(property.agent_id);
                List<Agent> agentList = (List<Agent>)await DataAgent.GetItemsAsync(agentId);
                agent = agentList[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
