﻿using ImpressionApp.Models;
using ImpressionApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ImpressionApp.ViewModels
{
    public class SearchFilterViewModel : BaseViewModel
    {
        //services instances
        public IDataStore<City> DataCity => DependencyService.Get<IDataStore<City>>() ?? new FilterCityTask();
        public IDataStore<Feature_Value> DataFeatureValue => DependencyService.Get<IDataStore<Feature_Value>>() ?? new FeatureValueTask();
        public IDataStore<Feature_Access> DataFeatureAccess => DependencyService.Get<IDataStore<Feature_Access>>() ?? new FeatureAccessTask();
        public IDataStore<Suburb> DataSuburb => DependencyService.Get<IDataStore<Suburb>>() ?? new FilterSuburbTask();

        //// basic filters
        ////public Dictionary<string, string> flt01_Cit_Dict { get; set; }
        public Dictionary<string, string> flt01_Sub_Dict { get; set; }
        public ObservableCollection<City> CityObList { get; set; }

        public ObservableCollection<Feature_Value> BedrObList { get; set; }
        public ObservableCollection<Feature_Value> BathObList { get; set; }
        public ObservableCollection<Feature_Value> PricObList { get; set; }
        public ObservableCollection<Feature_Value> SquaObList { get; set; }
        public ObservableCollection<Feature_Value> PoolObList { get; set; }
        public ObservableCollection<Feature_Value> RentObList { get; set; }
        public ObservableCollection<Feature_Value> CarObList { get; set; }
        public ObservableCollection<Feature_Value> GarObList { get; set; }

        private Boolean _boolAlarm = false;
        public Boolean BoolAlarm
        {
            get { return _boolAlarm; }
            set
            {
                _boolAlarm = value;
                OnPropertyChanged("BoolAlarm");
            }
        }

        private Boolean _boolPool = false;
        public Boolean BoolPool
        {
            get { return _boolPool; }
            set
            {
                _boolPool = value;
                OnPropertyChanged("BoolPool");
            }
        }

        private Boolean _boolAirConditioning = false;
        public Boolean BoolAirConditioning
        {
            get { return _boolAirConditioning; }
            set
            {
                _boolAirConditioning = value;
                OnPropertyChanged("BoolAirConditioning");
            }
        }

        private string _typeSelectedItem = string.Empty;
        public string TypeSelectedItem
        {
            get { return _typeSelectedItem; }
            set { SetProperty(ref _typeSelectedItem, value); }
        }

        Feature_Value _bedrSelectedItem = new Feature_Value();
        public Feature_Value BedrSelectedItem
        {
            get { return _bedrSelectedItem; }
            set { SetProperty(ref _bedrSelectedItem, value); }
        }

        Feature_Value _bathSelectedItem = new Feature_Value();
        public Feature_Value BathSelectedItem
        {
            get { return _bathSelectedItem; }
            set { SetProperty(ref _bathSelectedItem, value); }
        }

        Feature_Value _pricSelectedItem = new Feature_Value();
        public Feature_Value PricSelectedItem
        {
            get { return _pricSelectedItem; }
            set { SetProperty(ref _pricSelectedItem, value); }
        }

        Feature_Value squaSelectedItem = new Feature_Value();
        public Feature_Value SquaSelectedItem
        {
            get { return squaSelectedItem; }
            set { SetProperty(ref squaSelectedItem, value); }
        }
        
        Feature_Value _poolSelectedItem = new Feature_Value();
        public Feature_Value PoolSelectedItem
        {
            get { return _poolSelectedItem; }
            set { SetProperty(ref _poolSelectedItem, value); }
        }

        Feature_Value _rentSelectedItem = new Feature_Value();
        public Feature_Value RentSelectedItem
        {
            get { return _rentSelectedItem; }
            set { SetProperty(ref _rentSelectedItem, value); }
        }

        Feature_Value _garSelectedItem = new Feature_Value();
        public Feature_Value GarSelectedItem
        {
            get { return _garSelectedItem; }
            set { SetProperty(ref _garSelectedItem, value); }
        }

        Feature_Value _carSelectedItem = new Feature_Value();
        public Feature_Value CarSelectedItem
        {
            get { return _carSelectedItem; }
            set { SetProperty(ref _carSelectedItem, value); }
        }

        public ObservableCollection<Feature_Access> FeatureAccessObList { get; set; }

        public Command LoadFilterSuburbCommand { get; set; }
        public Command LoadFilterCityCommand { get; set; }
        public Command LoadFiltersCommand { get; set; }
        public Command LoadFeatureAccessCmd { get; set; }

        public SearchFilterViewModel()
        {
            Title = "Search";

            flt01_Sub_Dict = new Dictionary<string, string>();
            //flt01_Cit_Dict = new Dictionary<string, string>();
            CityObList = new ObservableCollection<City>();
            BedrObList = new ObservableCollection<Feature_Value>();
            BathObList = new ObservableCollection<Feature_Value>();
            PricObList = new ObservableCollection<Feature_Value>();

            SquaObList = new ObservableCollection<Feature_Value>();
            PoolObList = new ObservableCollection<Feature_Value>();
            RentObList = new ObservableCollection<Feature_Value>();
            CarObList = new ObservableCollection<Feature_Value>();
            GarObList = new ObservableCollection<Feature_Value>();

            //BoolAirConditioning = false;
            //BoolPool = false;
            //BoolAlarm = false;

            FeatureAccessObList = new ObservableCollection<Feature_Access>();

            LoadFeatureAccessCmd = new Command(async () => await ExecuteLoadFeatureAccess());

            LoadFilterSuburbCommand = new Command(async () => await ExecuteLoadFilterSuburb());
            LoadFilterCityCommand = new Command(async () => await ExecuteLoadFilterCity());

            LoadFiltersCommand = new Command(async () => await ExecuteLoadFilters());
        }

        async Task ExecuteLoadFeatureAccess()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {
                string usergroup = "3";
                if (CommonFunction.checkGlobalExist("IsLoggedIn"))
                {
                    usergroup = Application.Current.Properties["UserGroup"].ToString();
                }

                FeatureAccessObList = new ObservableCollection<Feature_Access>(
                    await DataFeatureAccess.GetItemsAsync(Convert.ToInt16(usergroup)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteLoadFilterSuburb()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                //flt01_Sub_Dict = new Dictionary<string, string>();
                flt01_Sub_Dict.Clear();

                //fetch data from service
                ////old version
                //List<Suburb> suburbs = (List<Suburb>)await DataSuburb.GetItemsAsync(2);
                ////add data to dictionary
                //foreach (var subitem in suburbs)
                //{
                //    flt01_Sub_Dict.Add("" + subitem.suburb_id, subitem.suburb_name);
                //}
                ////optimised 1
                //Suburb[] suburbs = (Suburb[])await DataSuburb.GetItemsAsync(2);
                //flt01_Sub_Dict = suburbs.ToDictionary(
                //                        item => item.suburb_id.ToString(),
                //                        item => item.suburb_name);
                //optimised 2
                flt01_Sub_Dict = ((Suburb[])await DataSuburb.GetItemsAsync(2)).ToDictionary(
                                        item => item.suburb_id.ToString(),
                                        item => item.suburb_name);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteLoadFilterCity()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {
                var cityArray = await DataCity.GetItemsAsync(true);
                foreach (var elm in cityArray)
                    CityObList.Add(elm);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteLoadFilters()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {
                var valueArray = await DataFeatureValue.GetItemsAsync(true);

                foreach (var elm in valueArray)
                {
                    switch (elm.feature_value)
                    {
                        case GlobalVar.FLT02_BEDROOM:
                            BedrObList.Add(elm);
                            break;
                        case GlobalVar.FLT03_BATHROOM:
                            BathObList.Add(elm);
                            break;
                        case GlobalVar.FLT04_PRICE:
                            PricObList.Add(elm);
                            break;

                        //advanced filers
                        case GlobalVar.FLT09_SQUAREM:
                            SquaObList.Add(elm);
                            break;
                        case GlobalVar.FLT12_SWIMPOOL:
                            PoolObList.Add(elm);
                            break;
                        case GlobalVar.FLT16_RENT:
                            RentObList.Add(elm);
                            break;
                        case GlobalVar.FLT20_CAR_PORTS:
                            CarObList.Add(elm);
                            break;
                        case GlobalVar.FLT21_GARAGES:
                            GarObList.Add(elm);
                            break;
                        default:
                            continue;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void ExecuteResetFilters()
        {
            BathSelectedItem = new Feature_Value();
            BedrSelectedItem = new Feature_Value();
            PricSelectedItem = new Feature_Value();
            RentSelectedItem = new Feature_Value();
            TypeSelectedItem = String.Empty;
            SquaSelectedItem = new Feature_Value();
            CarSelectedItem = new Feature_Value();
            GarSelectedItem = new Feature_Value();
            _boolAlarm = false;
            _boolPool = false;
            _boolAirConditioning = false;
        }
    }
}
