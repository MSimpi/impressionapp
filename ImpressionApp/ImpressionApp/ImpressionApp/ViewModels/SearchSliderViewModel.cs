﻿using FFImageLoading.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace ImpressionApp.ViewModels
{

    public class SearchSliderViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public SearchSliderViewModel()
        {
            MainImageSource = new ObservableCollection<View>()
            {
                 new CachedImage() {
                    WidthRequest = 300,
                    HeightRequest = 300,
                    CacheDuration = TimeSpan.FromDays(30),
                    DownsampleToViewSize = true,
                    RetryCount = 0,
                    RetryDelay = 250,
                    BitmapOptimizations = false,
                    LoadingPlaceholder = "loading.png",
                    ErrorPlaceholder = "no_image.jpg",
                    Aspect = Aspect.AspectFill,
                    Source = "https://impression.co.nz/assets/images/tmp/hb/banner-specialist-property-managers.jpg"
                },
                 new CachedImage() {
                    WidthRequest = 300,
                    HeightRequest = 300,
                    CacheDuration = TimeSpan.FromDays(30),
                    DownsampleToViewSize = true,
                    RetryCount = 0,
                    RetryDelay = 250,
                    BitmapOptimizations = false,
                    LoadingPlaceholder = "loading.png",
                    ErrorPlaceholder = "no_image.jpg",
                    Aspect = Aspect.AspectFill,
                    Source = "https://impression.co.nz/assets/images/tmp/hb/banner-bus-tours.jpg"
                },

                 new CachedImage() {
                    WidthRequest = 300,
                    HeightRequest = 300,
                    CacheDuration = TimeSpan.FromDays(30),
                    DownsampleToViewSize = true,
                    RetryCount = 0,
                    RetryDelay = 250,
                    BitmapOptimizations = false,
                    LoadingPlaceholder = "loading.png",
                    ErrorPlaceholder = "no_image.jpg",
                    Aspect = Aspect.AspectFill,
                    Source = "https://impression.co.nz/assets/images/tmp/hb/banner-live-rental-statistics.jpg"
                }
            };
        }

        ObservableCollection<View> _mainimageSource;

        public ObservableCollection<View> MainImageSource
        {
            set
            {
                _mainimageSource = value;
                OnPropertyChanged("MainImageSource");
            }
            get
            {
                return _mainimageSource;
            }
        }

        public Command MainImageCommand { protected set; get; }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
