﻿using ImpressionApp.Models;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Acr.UserDialogs;
using ImpressionApp.Services;
using System.Linq;
using System.Net;
using ImpressionApp.Views.Listing;
using MvvmHelpers;
using System.Collections.Generic;

namespace ImpressionApp.ViewModels
{
    public class PropertyListingViewModel : BaseViewModel
    {
        public ObservableRangeCollection<Property> ListAll { get; set; }
        public ObservableRangeCollection<Property> Lists { get; set; }   //display this
        public Command LoadPropertyCommand { get; set; }

        //comparison report
        public ObservableRangeCollection<Property> ListComparison { get; set; }
        public Command<Property> RemoveComparisonCommand
        {
            get
            {
                return new Command<Property>((property) =>
                {
                    if (string.Equals(property.unique_id, "Property ID"))
                    {
                        return;
                    }
                    ListComparison.Remove(property);
                });
            }
        }

        //filters
        public SearchFilterViewModel VM_Filter { get; set; }

        //sorters
        public ObservableRangeCollection<string> FilterOptions { get; }
        private string selectedFilter = "Featured";
        public string SelectedFilter
        {
            get => selectedFilter;
            set
            {
                if (SetProperty(ref selectedFilter, value))
                    SortItems();
            }
        }

        public PropertyListingViewModel()
        {
            Title = "For Sales";

            ListAll = new ObservableRangeCollection<Property>();
            Lists = new ObservableRangeCollection<Property>();

            //comparison report list
            ListComparison = new ObservableRangeCollection<Property>
            {
                //default labels
                GetLabel()
            };

            //get filters
            VM_Filter = GlobalVar.FilterViewModel;

            //sorters
            FilterOptions = new ObservableRangeCollection<string>
            {
                "Featured",
                "Lower Price",
                "Higher Price"
            };

            LoadPropertyCommand = new Command(async () => await ExecuteLoadProperty());

            MessagingCenter.Subscribe<Property>(this, "AddCpReport", (item) =>
            {
                var _item = item as Property;
                ListComparison.Add(_item);
            });
        }

        async Task ExecuteLoadProperty()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            //UserDialogs.Instance.ShowLoading();

            try
            {
                ListAll.ReplaceRange(await DataProperty.GetItemsAsync(0));
                FilterItems();
                SortItems();

                //show empty text
                IsEmptyList = Lists.Count == 0 ? true : false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
                //UserDialogs.Instance.HideLoading();
            }
        }

        void FilterItems()
        {
            var searchtxt = 
                Application.Current.Properties.ContainsKey("searchtext") ?
                Application.Current.Properties["searchtext"] : "";
            Lists.ReplaceRange
                (
                    ListAll.Where
                        (a =>
                            //search filters
                            (String.IsNullOrEmpty(VM_Filter.TypeSelectedItem) || VM_Filter.TypeSelectedItem == GlobalVar.FLTVALUE_ANY || Equals(a.type, VM_Filter.TypeSelectedItem))
                            && CheckRange(VM_Filter.BathSelectedItem, a.bathrooms)
                            && CheckRange(VM_Filter.BedrSelectedItem, a.bedrooms)
                            && CheckRange(VM_Filter.PricSelectedItem, a.price)
                            && CheckRange(VM_Filter.RentSelectedItem, a.rent_per_week)
                            && CheckRange(VM_Filter.SquaSelectedItem, a.square_meter)
                            && CheckRange(VM_Filter.CarSelectedItem, a.carports)
                            && CheckRange(VM_Filter.GarSelectedItem, a.garages)
                            && Equals(VM_Filter.BoolAirConditioning, !Equals("0", a.air_conditioning))
                            && Equals(VM_Filter.BoolAlarm, !Equals("0", a.alarm_system))
                            && Equals(VM_Filter.BoolPool, !Equals("0", a.pool))
                            //search text
                            //&& (String.IsNullOrEmpty(searchtxt.ToString())
                            //    || 
                            && ((a.title.ToLower().Contains(searchtxt?.ToString() ?? ""))
                                || (a.description.ToLower().Contains(searchtxt?.ToString() ?? ""))
                                || (a.fullAddress.ToLower().Contains(searchtxt?.ToString() ?? "")))
                        ).OrderByDescending(p => p.featured)
                );
        }

        void SortItems()
        {
            //"Featured",
            //    "Lower Price",
            //    "Higher Price"
            if (string.Equals(SelectedFilter, "Lower Price"))
            {
                IEnumerable<Property> li = Lists.OrderBy(p => p.price.Length).ThenBy(p => p.price).ToList();
                Lists.ReplaceRange(li);
            }
            else if (string.Equals(SelectedFilter, "Higher Price"))
            {
                IEnumerable<Property> li = Lists.OrderByDescending(p => p.price.Length).ThenBy(p => p.price).ToList();
                Lists.ReplaceRange(li);
            }
            else if (string.Equals(SelectedFilter, "Featured"))
            {
                IEnumerable<Property> li = Lists.OrderByDescending(p => p.featured).ToList();
                Lists.ReplaceRange(li);
            }
            else
            {

            }
        }

        Boolean CheckRange(Feature_Value filter, string value)
        {

            return 
                (filter == null)
                || (String.IsNullOrEmpty(filter.feature_text_value) 
                || filter.feature_text_value == GlobalVar.FLTVALUE_ANY) 
                    || (String.IsNullOrEmpty(filter.feature_key_value_low) ?
                        true :
                        value.CompareTo(filter.feature_key_value_low) >= 0
                            && value.CompareTo(filter.feature_key_value_high) <= 0)
                        ;
        }

        Boolean CheckRange(Feature_Value filter, int value)
        {
            return
                filter.feature_text_value == GlobalVar.FLTVALUE_ANY ||
                (filter.feature_key_value_low != "" && filter.feature_key_value_low != null ?
                    value.CompareTo(filter.feature_key_value_low) >= 0
                    && value.CompareTo(filter.feature_key_value_high) <= 0
                    : true)
                    ;
        }

        Property GetLabel()
        {
            Property label = new Property();
            label.unique_id = "Property ID";
            label.suburb = "Suburb";
            label.price = "Price";
            label.bedrooms = "Bedrooms";
            label.bathrooms = "Bathrooms";
            label.square_meter = "Square meter";
            label.type = "Type";
            label.carports = "Carports";
            label.garages = "Garages";
            label.pool = "Pool";
            label.rent_per_week = "Rent per week";
            label.id = 0;

            return label;
        }
    }
}