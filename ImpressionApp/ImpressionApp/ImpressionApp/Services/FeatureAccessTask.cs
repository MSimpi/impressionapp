﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using ImpressionApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

[assembly: Xamarin.Forms.Dependency(typeof(ImpressionApp.Services.FeatureAccessTask))]
namespace ImpressionApp.Services
{
    public class FeatureAccessTask : IDataStore<Feature_Access>
    {
        public FeatureAccessTask()
        {
        }

        public Task<bool> AddItemAsync(Feature_Access item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<Feature_Access> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Feature_Access>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }
        
        public async Task<IEnumerable<Feature_Access>> GetItemsAsync(int id)
        {
            //fetch data from DB API
            WebClient webClient = new WebClient();
            webClient.QueryString.Add(GlobalVar.TBL_FEATURE_ID, "" + id);
            var result = webClient.UploadValues(GlobalVar.PHP_FEATURE_ACCESS, "POST", webClient.QueryString);
            string str_json = Encoding.UTF8.GetString(result);

            //"[]" to IList
            IList<JToken> tokenList = JsonConvert.DeserializeObject<JToken[]>(str_json);
            IList<Feature_Access> results = new List<Feature_Access>();

            // serialize JSON list into .NET objects
            // reference link: https://www.newtonsoft.com/json/help/html/SerializingJSONFragments.htm
            foreach (JToken re in tokenList)
                // JToken.ToObject is a helper method that uses JsonSerializer internally
                results.Add(re.ToObject<Feature_Access>());

            return await Task.FromResult(results);
        }

        public Task<bool> UpdateItemAsync(Feature_Access item)
        {
            throw new NotImplementedException();
        }
    }
}