﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using ImpressionApp.Models;
using Newtonsoft.Json;

[assembly: Xamarin.Forms.Dependency(typeof(ImpressionApp.Services.FilterCityTask))]
namespace ImpressionApp.Services
{
    public class FilterCityTask : IDataStore<City>
    {
        public FilterCityTask()
        {
        }

        public Task<bool> AddItemAsync(City item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<City> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<City>> GetItemsAsync(bool forceRefresh = false)
        {
            //fetch data from DB API
            WebClient webClient = new WebClient();
            var result = webClient.DownloadString(GlobalVar.PHP_FILTER_CITY);

            return await Task.FromResult(JsonConvert.DeserializeObject<City[]>(result));
        }
        
        public Task<IEnumerable<City>> GetItemsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItemAsync(City item)
        {
            throw new NotImplementedException();
        }
    }
}