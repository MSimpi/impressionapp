﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Text;
using System.Security.Cryptography;
using ImpressionApp.Views;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using ImpressionApp.Models;
using System.Linq;
using System.Diagnostics;
using ImpressionApp.Views.Listing;
using ImpressionApp.ViewModels;
using Acr.UserDialogs;
using System.Net.Mail;
using ImpressionApp.Views.Auth;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace ImpressionApp.Services
{
    public static class CommonFunction
    {
        public static MainPage mainPage = new MainPage();

        static Regex EmailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        public static bool ValidateEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            return EmailRegex.IsMatch(email);
        }


        public static bool CheckingInternet()
        {
            var current = Connectivity.NetworkAccess;

            return current == NetworkAccess.Internet;
        }

        public static string GetHash256(string password, string salt)
        {
            byte[] passwordByte = ASCIIEncoding.ASCII.GetBytes(password + salt + "IREL");

            HashAlgorithm algorithm = SHA256.Create();
            byte[] hashPassword = algorithm.ComputeHash(passwordByte);
            return Convert.ToBase64String(hashPassword);
        }

        public static bool checkGlobalExist(string str)
        {
            if (Application.Current.Properties.ContainsKey(str))
            {
                if (Convert.ToBoolean(Application.Current.Properties[str]) == true)
                    return true;
            }
            return false;
        }

        public static bool sendEnquiryEmail(string fname, string lname, string email, string phone, string desc, string agentemail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress(email);

                if (agentemail == "")
                {
                    mail.To.Add(new MailAddress("impressioncityrentals@gmail.com"));
                }
                else
                {
                    mail.To.Add(new MailAddress(agentemail));
                }

                mail.Subject = "Enquiry - Mobile App";
                mail.Body = desc + "\n\n" +
                            "Name: " + fname + " " + lname + "\n" +
                            "Email: " + email + "\n" +
                            "Phone: " + phone;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("impressioncityrentals@gmail.com", "Impr355ion");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool sendFeedbackEmail(string fname, string lname, string email, string phone, string query, double rating, string desc)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress(email);
                mail.To.Add(new MailAddress("impressioncityrentals@gmail.com"));
                mail.Subject = "Feedback - Mobile App";
                mail.Body = desc + "\n\n" +
                            "Name: " + fname + " " + lname + "\n" +
                            "Email: " + email + "\n" +
                            "Phone: " + phone + "\n" +
                            "I have query regarding: " + query + "\n" +
                            "How do you feel about the app: " + rating;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("impressioncityrentals@gmail.com", "Impr355ion");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
                //DisplayAlert("Try Again", "Email Sending Falied "+ex.Message , "Ok");
            }
        }

        public static bool Logout()
        {
            if (checkGlobalExist("IsLoggedIn"))
            {
                Application.Current.Properties["IsLoggedIn"] = false;
                Application.Current.Properties["UserId"] = null;
                Application.Current.Properties["UserName"] = null;
                Application.Current.Properties["UserRole"] = null;
                return true;
            }
            return false;
        }

        public static string CreateRandomPassword(int length = 15)
        {
            // Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@_-";
            Random random = new Random();

            // Select one random character at a time from the string  
            // and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }

        public static bool sendForgotPasswordEmail(string email, string password)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("no-reply@impression.co.nz");
                mail.To.Add(new MailAddress(email));
                mail.Subject = "New Password - Impression Real Estate";
                mail.Body = "Your New Password: " + password;
                //"How do you feel about the app: "+ rating;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("simpimahesh8@gmail.com", "sairam123sairam");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
                //DisplayAlert("Try Again", "Email Sending Falied "+ex.Message , "Ok");
            }
        }

        async static public Task<Position> getCoordinates(List<Location> locationList, Xamarin.Forms.Maps.Map map, ItemsMapPage itmeMapPage)
        {
            Position position = new Position();
            try
            {
                foreach (Location locationData in locationList)
                {
                    string address = locationData.Address;
                    var locations = await Geocoding.GetLocationsAsync(address);

                    var location = locations?.FirstOrDefault();
                    if (location != null)
                    {
                        position = new Position(location.Latitude, location.Longitude);

                        var pin = new CustomPin
                        {
                            Type = (Xamarin.Forms.Maps.PinType)PinType.Place,
                            Position = position,
                            Label = locationData.Title,
                            Address = locationData.Address,
                            property = locationData.property
                            //Id = "Xamarin",
                            //Url = "http://xamarin.com/about/"
                        };

                        pin.Clicked += (sender, e) =>
                        {

                            if (itmeMapPage != null)
                                itmeMapPage.Marker_Clicked(sender, e);
                        };

                        map.Pins.Add(pin);


                        if (locationList.IndexOf(locationData) == locationList.Count - 1)
                        {
                            map.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromMiles(1)));
                        }

                    }
                }

            }
            catch (FeatureNotSupportedException fnsEx)
            {
                throw new Exception(fnsEx.Message);
                //await Navigation.PushAsync(new Pages.ErrorPage(fnsEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //await Navigation.PushAsync(new Pages.ErrorPage(ex.Message));
            }

            return position;
        }

        public static async void screenLoading(int sec)
        {
            using (UserDialogs.Instance.Loading("Loading", null, null, true, MaskType.Black))
            {
                await Task.Delay(sec);
            }
        }


        public static async void checkLocationPermission(RegisterPage registerPage, ProfilePage profilePage)
        {
            string message = "";

            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        message = "Gunna need that location";
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    status = results[Permission.Location];
                }

                if (status == PermissionStatus.Granted)
                {
                    message = "true";
                }
                else if (status != PermissionStatus.Unknown)
                {
                    message = "Please, try again.";
                }

                if (registerPage != null)
                {
                    registerPage.permissionResult(message);
                }
                else if (profilePage != null)
                {
                    profilePage.permissionResult(message);
                }
            }
            catch (Exception ex)
            {

                message = "Error: " + ex;
            }
        }

        public static async Task<Position> getUserLocation()
        {

            Position position = new Position();

            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                var location = await Geolocation.GetLastKnownLocationAsync();

                if (location != null)
                {
                    position = new Position(location.Latitude, location.Longitude);
                    //Console.WriteLine($"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                throw new Exception(fnsEx.Message);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                throw new Exception(fneEx.Message);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                throw new Exception(pEx.Message);
            }
            catch (Exception ex)
            {
                // Unable to get location
                throw new Exception(ex.Message);
            }

            return position;
        }



        public static async Task<Location> getAddress(Position position, RegisterPage registerPage, ProfilePage profilePage)
        {
            Location location = new Location();
            try
            {
                var placemarks = await Geocoding.GetPlacemarksAsync(position.Latitude, position.Longitude);

                var placemark = placemarks?.FirstOrDefault();
                if (placemark != null)
                {/*
                    var geocodeAddress =
                        $"AdminArea:       {placemark.AdminArea}\n" +
                        $"CountryCode:     {placemark.CountryCode}\n" +
                        $"CountryName:     {placemark.CountryName}\n" +
                        $"FeatureName:     {placemark.FeatureName}\n" +
                        $"Locality:        {placemark.Locality}\n" +
                        $"PostalCode:      {placemark.PostalCode}\n" +
                        $"SubAdminArea:    {placemark.SubAdminArea}\n" +
                        $"SubLocality:     {placemark.SubLocality}\n" +
                        $"SubThoroughfare: {placemark.SubThoroughfare}\n" +
                        $"Thoroughfare:    {placemark.Thoroughfare}\n";

                    Console.WriteLine(geocodeAddress);
                    */                   
                    location.Postcode = placemark.PostalCode;
                    location.Address = placemark.SubThoroughfare + ", " + placemark.Thoroughfare;
                    location.Suburb = placemark.SubLocality;
                    //Console.WriteLine("location.Suburb : "+ location.Postcode+", "+ location.Address+", "+ location.Suburb);
                    if(registerPage != null)
                    {
                        registerPage.submitRegister(location);
                    }
                    else if(profilePage != null)
                    {
                        profilePage.submitProfile(location);
                    }   
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                throw new Exception(fnsEx.Message);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                throw new Exception(fneEx.Message);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                throw new Exception(pEx.Message);
            }
            catch (Exception ex)
            {
                // Unable to get location
                throw new Exception(ex.Message);
            }

            return location;
        }

        static public bool IsCorrectMobileNumber(String strNumber)
        {
            Regex mobilePattern = new Regex(@"^[1-9]\d{10}$"); 
            return !mobilePattern.IsMatch(strNumber);
        }
    }

}
