﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using ImpressionApp.Models;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

[assembly: Xamarin.Forms.Dependency(typeof(ImpressionApp.Services.AgentTask))]
namespace ImpressionApp.Services
{
    public class AgentTask : IDataStore<Agent>
    {
        Agent agent;

        public AgentTask()
        {
            agent = new Agent();
        }

        public Task<bool> AddItemAsync(Agent item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<Agent> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Agent>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Agent>> GetItemsAsync(int id)
        {
            //set parameters
            //WebClient webClient = new WebClient();
            //webClient.QueryString.Add(GlobalVar.TBL_AGENT_ID, "" + id);

            //post request from PHP web API
            //var result = webClient.UploadValues(GlobalVar.PHP_PROPERTY_AGENT_DETAILS, "POST", webClient.QueryString);

            WebClient webClient = new WebClient();

            var result = webClient.DownloadString(GlobalVar.PHP_PROPERTY_AGENT_DETAILS);

            IEnumerable<Agent> jsonList = JsonConvert.DeserializeObject<IEnumerable<Agent>>(result);

            jsonList.ForEach(item =>
            {
                item.full_name = item.first_name + " " + item.last_name;
                item.thumb_image = "https://impression.co.nz" + item.thumb_image;

                item.content = item.content.Replace("<p>", "");
                item.content = item.content.Replace("</p>", "");
                item.content = item.content.Replace("&lt;br /&gt;", "\n");
                item.content = item.content.Replace("&lt;p&gt;", "'");
                item.content = item.content.Replace("&lt;/p&gt;", "'");
                item.content = item.content.Replace("&amp;&nbsp;", "");
                item.content = item.content.Replace("&nbsp;", "");

                item.quote = item.quote.Replace("&lt;p&gt;", "'");
                item.quote = item.quote.Replace("&lt;/p&gt;", "'");
                item.quote = item.quote.Replace("<p>", "");
                item.quote = item.quote.Replace("</p>", "");
            });

            //return array after Deserializing Json string
            return await Task.FromResult(jsonList.Where(x => x.id == id).ToList());
        }

        public Task<bool> UpdateItemAsync(Agent item)
        {
            throw new NotImplementedException();
        }

        Task<Agent> IDataStore<Agent>.GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

    }
}