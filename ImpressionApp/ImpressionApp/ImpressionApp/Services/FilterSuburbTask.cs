﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using ImpressionApp.Models;
using Newtonsoft.Json;

[assembly: Xamarin.Forms.Dependency(typeof(ImpressionApp.Services.FilterSuburbTask))]
namespace ImpressionApp.Services
{
    public class FilterSuburbTask : IDataStore<Suburb>
    {
        List<Suburb> suburbs;

        public FilterSuburbTask()
        {
            suburbs = new List<Suburb>();
        }

        public Task<bool> AddItemAsync(Suburb item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<Suburb> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Suburb>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Suburb>> GetItemsAsync(int id)
        {
            //set parameters
            WebClient webClient = new WebClient();
            webClient.QueryString.Add(GlobalVar.TBL_SUBURB_CITYID, "" + id);

            //post request from PHP web API
            var result = webClient.UploadValues(GlobalVar.PHP_FILTER_SUBURB, "POST", webClient.QueryString);

            //return array after Deserializing Json string
            return await Task.FromResult(JsonConvert.DeserializeObject<Suburb[]>(Encoding.UTF8.GetString(result)));
        }
        
        public Task<bool> UpdateItemAsync(Suburb item)
        {
            throw new NotImplementedException();
        }
    }
}