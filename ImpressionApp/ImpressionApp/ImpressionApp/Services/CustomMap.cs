﻿using System;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using ImpressionApp.Models;

namespace ImpressionApp.Services
{
    public class CustomMap : Map
    {
        public List<CustomPin> CustomPins { get; set; }
    }
}
