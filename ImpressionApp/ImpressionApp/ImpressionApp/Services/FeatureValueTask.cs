﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using ImpressionApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

[assembly: Xamarin.Forms.Dependency(typeof(ImpressionApp.Services.FeatureValueTask))]
namespace ImpressionApp.Services
{
    public class FeatureValueTask : IDataStore<Feature_Value>
    {
        public FeatureValueTask()
        {
        }

        public Task<bool> AddItemAsync(Feature_Value item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<Feature_Value> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Feature_Value>> GetItemsAsync(bool forceRefresh = false)
        {
            //fetch data from DB API
            WebClient webClient = new WebClient();
            var json_str = webClient.DownloadString(GlobalVar.PHP_FEATURE_VALUES);
            
            //"[]" to IList
            IList<JToken> tokenList = JsonConvert.DeserializeObject<JToken[]>(json_str);
            IList<Feature_Value> results = new List<Feature_Value>();

            // serialize JSON list into .NET objects
            // reference link: https://www.newtonsoft.com/json/help/html/SerializingJSONFragments.htm
            foreach (JToken re in tokenList)
                // JToken.ToObject is a helper method that uses JsonSerializer internally
                results.Add(re.ToObject<Feature_Value>());

            return await Task.FromResult(results);
        }
        
        public Task<IEnumerable<Feature_Value>> GetItemsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItemAsync(Feature_Value item)
        {
            throw new NotImplementedException();
        }
    }
}