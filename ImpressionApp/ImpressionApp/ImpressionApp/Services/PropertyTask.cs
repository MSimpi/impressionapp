﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using ImpressionApp.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(ImpressionApp.Services.PropertyTask))]
namespace ImpressionApp.Services
{
    public class PropertyTask : IDataStore<Property>
    {
        List<Property> properties;

        public PropertyTask()
        {
            properties = new List<Property>();
        }

        public Task<bool> AddItemAsync(Property item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<Property> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Property>> GetItemsAsync(bool forceRefresh = false)
        {
            List<Property> propertyList = new List<Property>();

            //fetch data from DB API
            WebClient webClient = new WebClient();

            var result = webClient.DownloadString(GlobalVar.PHP_PROPERTY_ALL);

            Property[] propertyarray = JsonConvert.DeserializeObject<Property[]>(result);

            foreach (Property proparray in propertyarray)
            {
                if (proparray.main_image != "")
                {
                    proparray.main_image = "https://impression.co.nz/" + proparray.main_image + "_lg.jpg";
                }
                propertyList.Add(proparray);
            }

            return await Task.FromResult(propertyList);
        }

        public async Task<IEnumerable<Property>> GetItemsAsync(int type)
        {
            if (type == 1)
            {
                //for favourite list
                return null;
            }
            else
            {
                WebClient webClient = new WebClient();

                var result = webClient.DownloadString(GlobalVar.PHP_PROPERTY_ALL);

                List<Property> jsonList = JsonConvert.DeserializeObject<List<Property>>(result);

                jsonList.ForEach(item =>
                {
                    item.title = item.title.Replace("&amp;", "&");
                    item.description = item.description.Replace("&lt;br /&gt;", "\n");
                    item.listAddress = item.street + ", " + item.suburb;
                    item.fullAddress = item.street_number + " " + item.street + ", " + item.suburb + ", " + item.region;
                    item.pricewithsymbol = "$" + string.Format("{0:0,0}", Convert.ToDouble(item.price));

                    if (item.main_image != "")
                    {
                        item.main_image = "https://impression.co.nz/" + item.main_image + "_lg.jpg";
                    }
                });

                return await Task.FromResult(jsonList);
            }
        }

        public Task<bool> UpdateItemAsync(Property item)
        {
            throw new NotImplementedException();
        }
    }
}