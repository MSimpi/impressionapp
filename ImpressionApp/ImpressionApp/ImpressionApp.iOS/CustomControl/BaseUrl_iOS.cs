﻿using System;
using Foundation;
using ImpressionApp.iOS.CustomControl;
using ImpressionApp.Views.Pages;
using Xamarin.Forms;

[assembly: Dependency(typeof(BaseUrl_iOS))]
namespace ImpressionApp.iOS.CustomControl
{
    public class BaseUrl_iOS : IBaseUrl
    {
        public string Get()
        {
            return NSBundle.MainBundle.BundlePath;
        }
    }
}
